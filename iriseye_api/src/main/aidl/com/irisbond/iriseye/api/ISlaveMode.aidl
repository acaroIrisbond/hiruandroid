
package com.irisbond.eviacam.api;

import com.irisbond.eviacam.api.IReadyEventListener;
import com.irisbond.eviacam.api.IGamepadEventListener;
import com.irisbond.eviacam.api.IMouseEventListener;
import com.irisbond.eviacam.api.IDockPanelEventListener;

/**
 * AIDL main interface for the eviacam slave mode
 */
interface ISlaveMode {
    /**
     * Triggers the initialization of the remote service. The initialization might take
     * an arbitrary amount of time (logo splash, user conditions agreement, etc.)
     * This method should be called ONLY ONCE, otherwise the behaviour is undefined.
     *
     * @param listener Listener that will be called once the initialization is completed.
     *                 This parameter is mandatory and cannot be null, in such a case, no
     *                 initialization will be performed.
     */
    void init(in IReadyEventListener listener);
    boolean start();
    void stop();

    void setOperationMode(in int mode);

    boolean registerGamepadListener (in IGamepadEventListener listener);
    void unregisterGamepadListener ();

    boolean registerMouseListener (in IMouseEventListener listener);
    void unregisterMouseListener ();

    boolean registerDockPanelListener (in IDockPanelEventListener listener);
    void unregisterDockPanelListener ();
}
