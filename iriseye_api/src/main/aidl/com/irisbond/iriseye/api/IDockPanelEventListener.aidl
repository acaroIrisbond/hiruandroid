
package com.irisbond.eviacam.api;

import android.view.MotionEvent;

/**
 * AIDL interface for the dock panel options
 */
oneway interface IDockPanelEventListener {
    /**
     * Options for the docking menu
     */
    const int BACK = 1;
    const int HOME = 2;
    const int RECENTS = 3;
    const int NOTIFICATIONS = 4;
    const int KEYBOARD = 5;
    const int CONTEXT_MENU = 6;
    const int REST_MODE = 7;

    void onDockMenuOption (in int option);
}
