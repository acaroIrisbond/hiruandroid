
package com.irisbond.eviacam.api;

/**
 * AIDL interface for the mouse listener
 */
oneway interface IReadyEventListener {
	void onReadyEvent (in boolean ready);
}
