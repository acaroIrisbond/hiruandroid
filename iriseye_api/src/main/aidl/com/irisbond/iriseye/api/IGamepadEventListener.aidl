
package com.irisbond.eviacam.api;

/**
 * AIDL interface for the gamepad listener
 */
oneway interface IGamepadEventListener {
	void buttonPressed (in int button);
	void buttonReleased (in int button);
}
