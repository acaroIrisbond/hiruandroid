
package com.irisbond.eviacam.api;

import android.view.MotionEvent;

/**
 * AIDL interface for the mouse listener
 */
oneway interface IMouseEventListener {
    void onMouseEvent (in MotionEvent e);
}
