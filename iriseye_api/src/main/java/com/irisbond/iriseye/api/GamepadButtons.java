
package com.irisbond.iriseye.api;

/**
 * Constants for the gamepad buttons
 */
public class GamepadButtons {
    /*
     * Sector identifiers (up to 8 sectors)
     * 
     * Don't touch the values, these are hardcoded
     */
    public static final int PAD_NONE = -1;
    public static final int PAD_DOWN = 0;
    public static final int PAD_DOWN_LEFT = 1;
    public static final int PAD_LEFT = 2;
    public static final int PAD_UP_LEFT = 3;
    public static final int PAD_UP = 4;
    public static final int PAD_UP_RIGHT = 5;
    public static final int PAD_RIGHT = 6;
    public static final int PAD_DOWN_RIGHT = 7;
}
