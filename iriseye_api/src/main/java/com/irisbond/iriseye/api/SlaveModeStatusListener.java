
package com.irisbond.iriseye.api;

/**
 * Interface for listening the status of the slave mode service
 */
public interface SlaveModeStatusListener {
    /***
     * Called when the remote IRISEYE service is connected and ready
     * @param sm reference to the service facade or null if null if something went wrong
     */
    void onReady(SlaveMode sm);
    void onDisconnected();
}
