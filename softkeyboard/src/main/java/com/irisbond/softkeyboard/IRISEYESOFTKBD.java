package com.irisbond.softkeyboard;

/***
 * Constants and common stuff
 */
class IRISEYESOFTKBD {
    public static final String TAG = IRISEYESOFTKBD.class.getSimpleName();

    private static final boolean DEBUG = BuildConfig.DEBUG;

    private static final boolean ATTACH_DEBUGGER = DEBUG;
}
