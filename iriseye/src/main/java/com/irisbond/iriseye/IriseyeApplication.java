
package com.irisbond.iriseye;

import android.app.Application;
import android.content.Context;
import android.os.Process;
import android.util.Log;

import com.irisbond.iriseye.common.Analytics;
import com.irisbond.iriseye.common.IRISEYE;
import com.irisbond.iriseye.common.UncaughtExceptionHandler;

/**
 * Customized application class
 */



public class IriseyeApplication extends Application {
    static Context context;
    static IriseyeApplication instance;
    private String host="192.168.0.22";

    public static Context getContext() {
        return context;
    }

    public void onCreate() {
        super.onCreate();

        Log.i(IRISEYE.TAG, "Application IRISEYE Eye Mouse started");
        context= getApplicationContext();
        instance=this;



        UncaughtExceptionHandler.init(this);



        /*
         * IRISEYE service regular start up (main process)
         */

        // Analytics.init(this);

        // Raise priority to improve responsiveness
        Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_DISPLAY);
    }

    public static IriseyeApplication getInstance() {
        return instance;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
