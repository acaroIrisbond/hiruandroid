package com.irisbond.iriseye.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.irisbond.iriseye.IrisbondCalibrationCommunication.IrisbondCalibration;
import com.irisbond.iriseye.IriseyeApplication;
import com.irisbond.iriseye.R;
import com.irisbond.iriseye.interfaces.OnMessageReceived;
import com.irisbond.iriseye.util.CustomSurfaceView;
import com.irisbond.iriseye.util.CustomView;
import com.irisbond.iriseye.util.GlobalSettings;
import com.irisbond.iriseye.util.HiruConfig;
import com.irisbond.iriseye.util.ProtoMessageManager;
import com.irisbond.iriseye.util.TcpClient;
import com.irisbond.iriseye.util.UdpServerThread;

import java.io.IOException;
import java.util.Random;

public class CalibrationActivity extends AppCompatActivity implements OnMessageReceived, CustomView.OnCalibrationAnimFinish {
    TcpClient tcpClient;
    UdpServerThread udpClient;
    RelativeLayout calibrationLayout;
    LinearLayout eyesLayout;
    LinearLayout eyesTrackLayout;
    Button calibrationButton;
    private String TAG="CalibrationActivity";


    boolean isCalibrating=false;
    ProtoMessageManager messageManager;

    CustomView imgCalibrationPoint;
    CustomSurfaceView customSurfaceView = null;
    private boolean distanceIsOK;
    SharedPreferences prefs;
    private ImageButton btnConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        /** Making this activity, full screen */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibration);
        setUI();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        DisplayMetrics displayMetrics = new DisplayMetrics();

        wm.getDefaultDisplay().getMetrics(displayMetrics);

        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        messageManager= new ProtoMessageManager(eyesTrackLayout.getWidth(),eyesTrackLayout.getHeight());
        udpClient= new UdpServerThread(HiruConfig.kDataPort,HiruConfig.kUDPBufferMaxSize,this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                udpClient.run();
            }
        }).start();
        prefs= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    private void setUI() {
        calibrationButton= findViewById(R.id.btnStartCalibration);
        calibrationLayout= findViewById(R.id.llCalibration);
        btnConfig= (ImageButton)findViewById(R.id.btnConfig);
        eyesLayout=findViewById(R.id.llEyes);
        eyesTrackLayout=findViewById(R.id.llEyePaintShape);
        imgCalibrationPoint=findViewById(R.id.imgCalibrationPoint);
        imgCalibrationPoint.setAnimFinishListener(this);
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        DisplayMetrics displayMetrics = new DisplayMetrics();

        wm.getDefaultDisplay().getMetrics(displayMetrics);

        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;
        customSurfaceView = new CustomSurfaceView(getApplicationContext(),width,height);
        // Add the custom surfaceview object to the layout.
        eyesTrackLayout.addView(customSurfaceView);
        calibrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(distanceIsOK){
                    messageManager= new ProtoMessageManager(width,height);
                    isCalibrating=true;
                    if(udpClient!=null && udpClient.isRunning()){
                        udpClient.disconnect();
                        udpClient.interrupt();
                    }
                    new ConnectTask().execute();


                    calibrationLayout.setVisibility(View.VISIBLE);
                    eyesLayout.setVisibility(View.GONE);
                    customSurfaceView.setVisibility(View.GONE);
                    IrisbondCalibration.Message calibration_start=messageManager.CreateMessage(IrisbondCalibration.message_identifier.calibration_start.getNumber(),null);
                    tcpClient.sendMessage(calibration_start);
                }else{
                    Toast.makeText(getApplicationContext(),getString(R.string.error_distance_eyes),Toast.LENGTH_LONG).show();
                }


            }
        });

        imgCalibrationPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IrisbondCalibration.Message message=messageManager.CreateMessage(IrisbondCalibration.message_identifier.calibration_start.getNumber(),null);
                tcpClient.sendMessage(message);
            }
        });
        btnConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                configDialog();
            }
        });

    }

    private void configDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CalibrationActivity.this);
        alertDialog.setTitle(getString(R.string.new_host_dialog_tittle));
        alertDialog.setMessage(getString(R.string.new_host_dialog_message));

        final EditText input = new EditText(CalibrationActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);


        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String newHost = input.getText().toString();
                        if (newHost.compareTo("") == 0) {
                            prefs.edit().putString("host",newHost);
                            CalibrationActivity.this.recreate();
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }



    @Override
    public void messageReceived(IrisbondCalibration.Message message) {
        Log.d(TAG,"message received messaid:"+message.getMessageId());
        if(isCalibrating){
            if(message.getMessageId().getNumber()== IrisbondCalibration.message_identifier.new_calib_coords_VALUE){
                if(message.getData()!=null){

                    IrisbondCalibration.msg_new_calib_coords msg = (IrisbondCalibration.msg_new_calib_coords) messageManager.readData(message);
                    if(msg.getFixated()){
                        imgCalibrationPoint.fixation(HiruConfig.kFixationime);
                    }else{


                        final int height = calibrationLayout.getHeight();
                        final int width = calibrationLayout.getWidth();
                        int coordX=msg.getCoordX();
                        int coordY=msg.getCoordY();
                        if(msg.getCoordX()>width){
                            coordX=width-50;
                        }
                        if(coordY>height){
                            coordY=height-50;
                        }
                        if(coordX<100){
                            coordX=120;
                        }
                        if(coordY<100){
                            coordY=120;
                        }
                        Log.v(TAG,"calibration coords X:"+coordX+" y:"+coordY+" height:"+height+" width:"+width);
                        imgCalibrationPoint.move(coordX,coordY,HiruConfig.kTravelTime);
                    }
                    IrisbondCalibration.Message coordsReceived=messageManager.CreateMessage(IrisbondCalibration.message_identifier.coords_received.getNumber(),null);
                    tcpClient.sendMessage(coordsReceived);

                }
            }else if(message.getMessageId().getNumber()== IrisbondCalibration.message_identifier.ok_start_calib_VALUE){

                IrisbondCalibration.Message startGUI=messageManager.CreateMessage(IrisbondCalibration.message_identifier.gui_started.getNumber(),null);
                tcpClient.sendMessage(startGUI);

            }else if(message.getMessageId().getNumber()== IrisbondCalibration.message_identifier.gui_started_VALUE){


                IrisbondCalibration.Message pointReady=messageManager.CreateMessage(IrisbondCalibration.message_identifier.point_ready.getNumber(),null);
                tcpClient.sendMessage(pointReady);
            }else if(message.getMessageId().getNumber()== IrisbondCalibration.message_identifier.calib_finished_VALUE){
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putBoolean(GlobalSettings.CALIBRATION_DONE,true).apply();
                setResult(RESULT_OK);
                CalibrationActivity.this.finish();
            }
        }else{
            if(message.getMessageId().getNumber()== IrisbondCalibration.message_identifier.data_VALUE){
                if(message.getData()!=null){

                    final IrisbondCalibration.msg_data msgData=(IrisbondCalibration.msg_data) messageManager.readData(message);
                    Point[] coordsNorm=new Point[2];

                    if(customSurfaceView==null){
                        coordsNorm[0]=new Point(0,0);
                        coordsNorm[1]=new Point(0,0);
                    }else {

                        if (msgData.getLeftEyeDetected()) {
                            coordsNorm[0] = new Point((int) (msgData.getLeftEyeX() * customSurfaceView.getWidth() / msgData.getImageWidth()), (int) (msgData.getLeftEyeY() * customSurfaceView.getHeight() / msgData.getImageHeight()));
                        } else {
                            coordsNorm[0] = new Point(0, 0);
                        }
                        if (msgData.getRightEyeDetected()) {
                            coordsNorm[1] = new Point((int) (msgData.getRightEyeX() * customSurfaceView.getWidth() / msgData.getImageWidth()), (int) (msgData.getRightEyeY() * customSurfaceView.getHeight() / msgData.getImageHeight()));

                        } else {
                            coordsNorm[1] = new Point(0, 0);

                        }
                    }

                    Log.d(TAG,"coords0" + coordsNorm[0] + "coords1"+ coordsNorm[1]);

                   /* DEVELOPMENT DATA
                   int randomFactor= new Random().nextInt(100) + 20;
                    coordsNorm[0]=new Point(200+randomFactor,200+randomFactor);
                    coordsNorm[1]=new Point(350+randomFactor,200+randomFactor);
                    */
                    final boolean[] detection= new boolean[2];
                    detection[0]=msgData.getLeftEyeDetected();
                    detection[1]=msgData.getRightEyeDetected();
                    Log.d(TAG,"detection[0]" + detection[0] + "detection[1]"+ detection[1]);

                    /*DEVELOPMENT DATA

                    detection[0]=new Random().nextInt(2) == 0;
                    detection[1]=new Random().nextInt(2) == 0;
                    */
                    final Point[] coords=coordsNorm;
                    try {
                        if (msgData.getDistanceFactor() >= -0.7 && msgData.getDistanceFactor() <= 1.2) {
                            distanceIsOK=true;

                        } else {
                            distanceIsOK=false;
                        }
                        customSurfaceView.drawEyes(coords,detection,msgData.getDistanceFactor());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }



            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            IrisbondCalibration.Message calibrationCancel=messageManager.CreateMessage(IrisbondCalibration.message_identifier.calibration_cancel.getNumber(),null);
            tcpClient.sendMessage(calibrationCancel);
            if(tcpClient!=null && tcpClient.ismRun()){

                tcpClient.stopClient();

            }
            if(udpClient!=null && udpClient.isRunning()){

                udpClient.interrupt();
                udpClient.disconnect();

            }
        } catch (Exception e) {
            Log.e(TAG,e.getMessage());
        }
    }

    @Override
    public void onAnimEnd() {
        IrisbondCalibration.Message pointReady=messageManager.CreateMessage(IrisbondCalibration.message_identifier.point_ready.getNumber(),null);
        tcpClient.sendMessage(pointReady);
    }

    public class ConnectTask extends AsyncTask<String, IrisbondCalibration.Message, TcpClient> {

        @Override
        protected TcpClient doInBackground(String... message) {

            //we create a TCPClient object
            tcpClient = new TcpClient(new OnMessageReceived() {
                @Override
                public void messageReceived(IrisbondCalibration.Message message) {
                    publishProgress(message);
                }

            }, prefs.getString("host",HiruConfig.kHost),HiruConfig.kCalibrationPort,HiruConfig.kTCPBufferMaxSize);
            tcpClient.run();


            return null;
        }

        @Override
        protected void onProgressUpdate(IrisbondCalibration.Message... values) {
            super.onProgressUpdate(values);
            //response received from server
            Log.d("test", "response " + values[0]);
            CalibrationActivity.this.messageReceived(values[0]);

        }
    }

}
