
package com.irisbond.iriseye.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.irisbond.iriseye.R;
import com.irisbond.iriseye.service.IrisbondAccessibilityService;
import com.irisbond.iriseye.common.Preferences;
import com.irisbond.iriseye.util.GlobalSettings;

/**
 * Launcher activity
 */

public class LauncherActivity extends Activity {

    private static final String TAG =LauncherActivity.class.getSimpleName() ;
    private static final int CALIBRATION_INTENT = 101;
    /* Keep references to the dialogs to properly dismiss them. See:
           http://stackoverflow.com/questions/2850573/activity-has-leaked-window-that-was-originally-added */
    Dialog mHelpDialog;
    Dialog mNoA11ySettingsDialog;
    private int mDeviceOrientation;
    private int mDeviceRotation;
    private static final int MY_PERMISSIONS_REQUEST_PHONE_STATE = 1;
    private static final String[] APP_PERMISSIONS = {

            Manifest.permission.READ_PHONE_STATE,

    };


    /**
     * Display message for no accessibility settings available
     */
    private void noAccessibilitySettingsAlert() {
        final Resources r= getResources();
        mNoA11ySettingsDialog= new AlertDialog.Builder(this)
            .setMessage(r.getText(R.string.launcher_no_accessibility_settings))
            .setPositiveButton(r.getText(R.string.launcher_done),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                           // dialogInterface.dismiss();
                            //finish();
                        }
                    })
            .create();
        mNoA11ySettingsDialog.setCancelable(false);
        mNoA11ySettingsDialog.setCanceledOnTouchOutside(false);
        mNoA11ySettingsDialog.show();
    }

    /**
     * Open the accessibility settings screen
     *
     * @return true on success
     */
    private boolean openAccessibility() {
        Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        try {
            startActivity(intent, null);
        }
        catch(ActivityNotFoundException e) {
            return false;
        }
        return true;
    }

    private void showLauncherHelp () {
        View checkBoxView = View.inflate(this, R.layout.launcher_help, null);
        CheckBox checkBox = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Preferences.get().setShowLauncherHelp(!isChecked);
            }
        });

        Resources r= getResources();
        mHelpDialog= new AlertDialog.Builder(this)
        .setMessage(r.getText(R.string.launcher_how_to_run))
        .setView(checkBoxView)
        .setPositiveButton(r.getText(R.string.launcher_open_a11y_settings),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        if (!openAccessibility()) noAccessibilitySettingsAlert();
                    }
                })
        .create();
        mHelpDialog.setCancelable(false);
        mHelpDialog.setCanceledOnTouchOutside(false);
        mHelpDialog.show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(!prefs.getBoolean(GlobalSettings.CALIBRATION_DONE,false)){
            Intent calibrationIntent= new Intent(getApplicationContext(),CalibrationActivity.class);
            startActivityForResult(calibrationIntent,CALIBRATION_INTENT);
        }else{
            int permissionPhone = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

            if (permissionPhone != PackageManager.PERMISSION_GRANTED  ) {

                startAppFlow();
            }else{
                requestPermissions();
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();




    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, APP_PERMISSIONS, MY_PERMISSIONS_REQUEST_PHONE_STATE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        int granted = 0, rejected = 0;

        if (requestCode == MY_PERMISSIONS_REQUEST_PHONE_STATE) {
            // for each permission check if the user granted/denied them
            // you may want to group the rationale in a single dialog,
            // this is just an example
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                    if (! showRationale) {
                        // user also CHECKED "never ask again"
                        // you can either enable some fall back,
                        // disable features of your app
                        // or open another dialog explaining
                        // again the permission and directing to
                        // the app setting
                    } else {
                        //if (!mConfirmUserUnderstoodPermission)
                        //    showCameraPermissionConfirmDialog();

                        // user did NOT check "never ask again"
                        // this is a good place to explain the user
                        // why you need the permission and ask if he wants
                        // to accept it (the rationale)
                        Log.i(TAG, ">> permission ["+permission+"] rejected");
                        rejected++;
                    }
                } else {
                    //User has granted the permissions.
                    Log.i(TAG, ">> Permission ["+permission+"] granted");
                    granted++;
                }
            }
            Log.i(TAG, ">> Granted: "+granted+" Rejected: "+rejected+" Total: "+permissions.length);
            if (granted == permissions.length) {
                Log.i(TAG, ">> All permissions granted. Ready to start service");
               // startService();
                startAppFlow();
            } else {
                Log.i(TAG, ">> One or more permissions where not granted. Finish the app.");

            }
        }
    }

    private void startService() {

            Log.i(TAG, ">>>> Starting the service...");
        getDeviceOrientation();
        getDeviceRotation();


           // mWebTrackerCore.setup(getApplicationContext().getPackageName(), "43307a95-3aff-4d20-88b7-f7c2db25008c", mDeviceOrientation, mDeviceRotation);

    }
    private void getDeviceOrientation() {
        int orientation = getResources().getConfiguration().orientation;

        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.i(TAG,"Configuration.ORIENTATION_LANDSCAPE");
            mDeviceOrientation = 2;

        } else if(orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.i(TAG, "Configuration.ORIENTATION_PORTRAIT");
            mDeviceOrientation = 1;
        }
    }

    private void getDeviceRotation() {
        mDeviceRotation = computeDeviceRotation();
    }

    private int computeDeviceRotation() {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        int rotation = windowManager.getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return 0;
            case Surface.ROTATION_90:
                return 90;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_270:
                return 270;
        }
        return 0;
    }



    @Override
    protected void onPause() {
        super.onPause();

        if (mHelpDialog!= null) {
            mHelpDialog.dismiss();
            mHelpDialog= null;
        }

        if (mNoA11ySettingsDialog!= null) {
            mNoA11ySettingsDialog.dismiss();
            mNoA11ySettingsDialog= null;
        }

        if(Preferences.get()!=null)Preferences.get().cleanup();

    //    finish();
    }



    private void onServiceReady() {

        startAppFlow();
    }

    private void startAppFlow() {
        if (Preferences.initForA11yService(this) == null) return;

        IrisbondAccessibilityService service= IrisbondAccessibilityService.get();
        if (null != service) {
            /* Engine running, open notifications */
            service.openNotifications();
            finish();
        }
        else {
            Preferences.get().setShowLauncherHelp(true);
            if (Preferences.get().getShowLauncherHelp()) {
                showLauncherHelp();
            } else {
                if (!openAccessibility()) noAccessibilitySettingsAlert();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==CALIBRATION_INTENT){
            if(resultCode!=RESULT_OK){
                Intent calibrationIntent= new Intent(getApplicationContext(),CalibrationActivity.class);
                startActivityForResult(calibrationIntent,CALIBRATION_INTENT);
            }else{
                int permissionCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
                int permissionPhone = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

                if (permissionCamera != PackageManager.PERMISSION_GRANTED && permissionPhone != PackageManager.PERMISSION_GRANTED  ) {
                    startAppFlow();
                }else{
                    requestPermissions();
                }
            }
        }
    }
}
