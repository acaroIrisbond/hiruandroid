
package com.irisbond.iriseye.interfaces;

import android.graphics.PointF;
import android.support.annotation.NonNull;

/**
 * Interface for mouse emulation callbacks
 */

public interface MouseEmulationCallbacks {

    /**
     * Called each time a mouse event is generated, either motion event and/or click event
     * @param location location of the pointer is screen coordinates
     * @param click true when click generated
     */
    void onMouseEvent(@NonNull PointF location, boolean click);

    /**
     * Called to ask whether a certain location of the screen is clickable.
     * It is used to enable/disable the dwell click countdown
     *
     * @param location location of the pointer is screen coordinates
     * @return true if the point is clickable
     */
    boolean isClickable(@NonNull PointF location);
}
