package com.irisbond.iriseye.interfaces;

import com.irisbond.iriseye.IrisbondCalibrationCommunication.IrisbondCalibration;

public interface OnMessageReceived {
        public void messageReceived(IrisbondCalibration.Message message);
    }