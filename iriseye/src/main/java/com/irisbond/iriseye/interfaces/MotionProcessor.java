

package com.irisbond.iriseye.interfaces;

import android.graphics.PointF;
import android.support.annotation.NonNull;

public interface MotionProcessor {
    void processMotion(@NonNull PointF motion);
    void start();
    void stop();
    void cleanup();
}