

package com.irisbond.iriseye.interfaces;

import android.support.annotation.NonNull;
import android.view.accessibility.AccessibilityEvent;

import com.irisbond.iriseye.interfaces.Engine;

/**
 * Interface for the engine in accessibility service mode
 */
public interface AccessibilityServiceModeEngine extends Engine {
    /**
     * Enable/disable on-screen pointer
     */
    void enablePointer();
    void disablePointer();

    /**
     * Enable/disable dwell based click action
     */
    void enableClick();
    void disableClick();

    /**
     * Show/hide the docking menu panel
     */
    void enableDockPanel();
    void disableDockPanel();

    /**
     * Enable/disable scroll buttons
     */
    void enableScrollButtons();
    void disableScrollButtons();

    /**
     * Enable all above elements
     */
    void enableAll();

    /**
     * Method to route the accessibility events received by the accessibility service
     * @param event the accessibility event
     */
    void onAccessibilityEvent(@NonNull AccessibilityEvent event);
}
