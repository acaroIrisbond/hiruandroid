
package com.irisbond.iriseye;

import com.irisbond.iriseye.interfaces.AccessibilityServiceModeEngine;
import com.irisbond.iriseye.service.AccessibilityServiceModeEngineImpl;

/**
 * Class which allows to select among of the available engine modes. Currently:
 *   * Accessibility service mode: this is the main mode for IRISEYE and can only be started from
 *     an accessibility service
 *   * Slave mode: in this mode, IRISEYE can be run using its API
 *
 * Both modes CANNOT run simultaneously
 */
public class EngineSelector {
    /* singleton instances */
    private static AccessibilityServiceModeEngine sAccessibilityServiceModeEngine = null;


    /**
     * Get an instance to the current accessibility mode engine
     *
     * @return a reference to the engine interface or null if not available
     */
    public static AccessibilityServiceModeEngine initAccessibilityServiceModeEngine() {


        if (null != sAccessibilityServiceModeEngine) {
            throw new IllegalStateException(
                    "initAccessibilityServiceModeEngine: already initialized");
        }
        sAccessibilityServiceModeEngine= new AccessibilityServiceModeEngineImpl();

        return sAccessibilityServiceModeEngine;
    }

    public static AccessibilityServiceModeEngine getAccessibilityServiceModeEngine() {
        return sAccessibilityServiceModeEngine;
    }

    /**
     * Release accessibility service mode if previously requested
     */
    public static void releaseAccessibilityServiceModeEngine() {
        if (null != sAccessibilityServiceModeEngine) {
            sAccessibilityServiceModeEngine.cleanup();
            sAccessibilityServiceModeEngine = null;
        }
    }


}
