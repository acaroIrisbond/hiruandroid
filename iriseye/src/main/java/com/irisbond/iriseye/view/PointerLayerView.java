

package com.irisbond.iriseye.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.View;

import com.irisbond.iriseye.R;
import com.irisbond.iriseye.util.Settings;


/**
 * Layout to draw click progress feedback and pointer
 *
 */

public class PointerLayerView extends View implements OnSharedPreferenceChangeListener {

    // Size of the long side of the pointer for normal size (in DIP)
    private static final float CURSOR_LONG_SIDE_DIP = 30;

    // Radius of the visual progress feedback (in DIP)
    private static final float PROGRESS_INDICATOR_RADIUS_DIP = 30;

    // default alpha value
    private static final int DEFAULT_ALPHA= 255;
    private final int DISABLED_ALPHA;

    // cached paint box
    private final Paint mPaintBox;
    private final Paint mCirclePaint;
    private final Paint mEraserPaint;
    private final SharedPreferences sp;


    // the location where the pointer needs to be painted
    private PointF mPointerLocation;

    // alpha value for drawing pointer
    private int mAlphaPointer= DEFAULT_ALPHA;

    // bitmap of the (mouse) pointer
    private Bitmap mPointerBitmap;

    // progress indicator radius in px
    private float mProgressIndicatorRadius;

    // click progress percent so far (0 disables)
    private int mClickProgressPercent= 0;
    private float mCircleSweepAngle;

    public PointerLayerView(Context c) {
        super(c);

        mPaintBox = new Paint();
        setWillNotDraw(false);
        mPointerLocation= new PointF();

        DISABLED_ALPHA= c.getResources().getColor(R.color.disabled_alpha) >> 24;
        mCirclePaint = new Paint();
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setColor(circleColor);

        mEraserPaint = new Paint();
        mEraserPaint.setAntiAlias(true);
        mEraserPaint.setColor(Color.TRANSPARENT);
        mEraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        // preferences
        sp= PreferenceManager.getDefaultSharedPreferences(c);
        sp.registerOnSharedPreferenceChangeListener(this);
        updateSettings();
    }

    public void cleanup() {
        sp.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        if (key.equals(Settings.KEY_UI_ELEMENTS_SIZE) ) {
            updateSettings();
        }
    }

    private void updateSettings() {
        float size= 1;

        mAlphaPointer= (255 * 50) / 100;

        // re-scale pointer accordingly
        BitmapDrawable bd = (BitmapDrawable)
                getContext().getResources().getDrawable(R.drawable.pointer);
        Bitmap origBitmap= bd.getBitmap();
        origBitmap.setDensity(Bitmap.DENSITY_NONE);

        // desired long side in pixels of the pointer for this screen density
        float longSide= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                CURSOR_LONG_SIDE_DIP, getResources().getDisplayMetrics()) * size;
        float scaling = longSide / (float) bd.getIntrinsicHeight();
        float shortSide= scaling * bd.getIntrinsicWidth();

        mPointerBitmap= Bitmap.createScaledBitmap(origBitmap, (int) shortSide, (int) longSide, true);
        mPointerBitmap.setDensity(Bitmap.DENSITY_NONE);

        // compute radius of progress indicator in px
        mProgressIndicatorRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                PROGRESS_INDICATOR_RADIUS_DIP, getResources().getDisplayMetrics()) * size;

        final float thickness = getWidth() * THICKNESS_SCALE;

        mCircleOuterBounds = new RectF(mPointerLocation.x,mPointerLocation.y,bd.getIntrinsicWidth(), bd.getIntrinsicHeight());
        mCircleInnerBounds = new RectF(
                mCircleOuterBounds.left + thickness,
                mCircleOuterBounds.top + thickness,
                mCircleOuterBounds.right - thickness,
                mCircleOuterBounds.bottom - thickness);

    }
    int circleColor = Color.GREEN;
    private RectF mCircleOuterBounds;
    private RectF mCircleInnerBounds;
    private static final int ARC_START_ANGLE = 0; // 12 o'clock

    private static final float THICKNESS_SCALE = 0.0015f;
    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        final float thickness = getWidth() * THICKNESS_SCALE;

        mCircleOuterBounds = new RectF(mPointerLocation.x,mPointerLocation.y,mPointerLocation.x+100, mPointerLocation.y+100);
        mCircleInnerBounds = new RectF(
                mCircleOuterBounds.left + thickness,
                mCircleOuterBounds.top + thickness,
                mCircleOuterBounds.right - thickness,
                mCircleOuterBounds.bottom - thickness);
        // draw progress indicator
        if (mClickProgressPercent> 0 && false) {
            float radius= ((float)
                    (100 - mClickProgressPercent) * mProgressIndicatorRadius) / 100.0f;

            mPaintBox.setStyle(Paint.Style.FILL_AND_STROKE);
            mPaintBox.setColor(0x80000000);
            canvas.drawCircle(mPointerLocation.x, mPointerLocation.y, radius, mPaintBox);

            mPaintBox.setStyle(Paint.Style.STROKE);
            mPaintBox.setColor(0x80FFFFFF);
            canvas.drawCircle(mPointerLocation.x, mPointerLocation.y, radius, mPaintBox);
        }
        if (mCircleSweepAngle > 0f) {

            canvas.drawArc(mCircleOuterBounds, ARC_START_ANGLE, mCircleSweepAngle, true, mCirclePaint);
            canvas.drawOval(mCircleInnerBounds, mEraserPaint);
        }

        // draw pointer
        mPaintBox.setAlpha(mAlphaPointer);
        mCirclePaint.setAlpha(mAlphaPointer);

        canvas.drawBitmap(mPointerBitmap, mPointerLocation.x, mPointerLocation.y, mPaintBox);
    }

    public void updatePosition(PointF p) {
        mPointerLocation.x= p.x;
        mPointerLocation.y= p.y;
    }

    public void updateClickProgress(int percent) {
        mClickProgressPercent= percent;
        if(percent>0) mCircleSweepAngle = 360 * ((float)percent/100f);

        if(percent >0 && percent<40 ){
            circleColor=Color.GREEN;


        }else if(percent>40 && percent<60){
            circleColor=Color.YELLOW;


        }else if(percent>60){
            circleColor=Color.RED;


        }else if(percent>=100 || percent<=0){
            circleColor=Color.TRANSPARENT;
        }
        mCirclePaint.setColor(circleColor);
    }

    /**
     * Enable or disable faded appearance of the pointer for the rest mode
     *
     * @param value true for faded appearance
     */
    public void setRestModeAppearance(boolean value) {
        mAlphaPointer= (value? DISABLED_ALPHA : DEFAULT_ALPHA);
    }
}
