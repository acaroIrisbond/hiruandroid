

 package com.irisbond.iriseye.view;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.graphics.PixelFormat;

import com.irisbond.iriseye.common.IRISEYE;

/***
 * Root relative layout which is overlaid over the entire screen of the device an to which
 * other views can be added.
 */
public class OverlayView extends RelativeLayout {

    public OverlayView(Context c) {
        super(c);
        
        WindowManager.LayoutParams feedbackParams = new WindowManager.LayoutParams();
        
        feedbackParams.setTitle("FeedbackOverlay");
        
        // Transparent background
        feedbackParams.format = PixelFormat.TRANSPARENT;
        
        /*
         * Type of window- Create an always on top window
         * 
         * TYPE_PHONE: These are non-application windows providing user interaction with the
         *      phone (in particular incoming calls). These windows are normally placed above 
         *      all applications, but behind the status bar. In multiuser systems shows on all 
         *      users' windows.
         *      
         * TYPE_SYSTEM_OVERLAY: system overlay windows, which need to be displayed on top of 
         *      everything else. These windows must not take input focus, or they will interfere 
         *      with the keyguard. In multiuser systems shows only on the owning user's window
         *      
         * TODO: For future versions check TYPE_ACCESSIBILITY_OVERLAY
         */
        int layout_parms;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)

        {
            layout_parms = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;

        }

        else {

            layout_parms = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;

        }
        feedbackParams.type =layout_parms;
               
        /*
         * Type of window. Whole screen is covered (including status bar)
         * 
         * FLAG_NOT_FOCUSABLE: this window won't ever get key input focus, so the user can not 
         *      send key or other button events to it. It can use the full screen for its content 
         *      and cover the input method if needed
         *      
         * FLAG_LAYOUT_IN_SCREEN: place the window within the entire screen, ignoring decorations 
         *      around the border (such as the status bar)
         *             
         */
        feedbackParams.flags =
                //WindowManager.LayoutParams.TYPE_PRIORITY_PHONE | WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;

        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE  |
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE  |
                // this is to enable the notification to receive touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        feedbackParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        feedbackParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        this.setSystemUiVisibility(uiOptions);
        this.setClickable(false);
        this.setFocusable(false);
        this.setTouchscreenBlocksFocus(false);

        WindowManager wm= (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        wm.addView(this, feedbackParams);

    }
   
    public void cleanup() {
        WindowManager wm= (WindowManager) this.getContext().getSystemService(Context.WINDOW_SERVICE);
        wm.removeViewImmediate(this);

        Log.i(IRISEYE.TAG, "OverlayView: finish destroyOverlay");
    }
    
    public void addFullScreenLayer (View v) {
        RelativeLayout.LayoutParams lp= new RelativeLayout.LayoutParams(this.getWidth(), this.getHeight());
        lp.width= RelativeLayout.LayoutParams.MATCH_PARENT;
        lp.height= RelativeLayout.LayoutParams.MATCH_PARENT;
        
        v.setLayoutParams(lp);
        this.addView(v);
    }
}
