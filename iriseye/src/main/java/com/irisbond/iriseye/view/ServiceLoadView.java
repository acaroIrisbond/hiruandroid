package com.irisbond.iriseye.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.irisbond.iriseye.R;

/**
 * Created by javier on 17/11/17.
 */

public class ServiceLoadView extends LinearLayout {
    private Context mContext;
    private View mRootView;
    private TextView mLoadingTextView;
    private ProgressBar mLoadingProgressBar;

    public ServiceLoadView(Context context) {
        super(context);
        init(context);
    }

    public ServiceLoadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ServiceLoadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ServiceLoadView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void startLoadService() {
        mLoadingTextView.setVisibility(View.VISIBLE);
        mLoadingProgressBar.setVisibility(View.VISIBLE);
    }

    public void stopLoadService() {
        mLoadingTextView.setVisibility(View.INVISIBLE);
        mLoadingProgressBar.setVisibility(View.INVISIBLE);
    }

    private void init(Context context) {
        mContext = context;
        mRootView = inflate(context, R.layout.view_service_load, this);
        mLoadingTextView = (TextView) mRootView.findViewById(R.id.tv_loading);
        mLoadingProgressBar = (ProgressBar) mRootView.findViewById(R.id.pb_loading);
    }
}
