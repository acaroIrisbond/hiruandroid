
package com.irisbond.iriseye.service;

import com.irisbond.iriseye.common.CoreEngine;
import com.irisbond.iriseye.common.FaceDetectionCountdown;
import com.irisbond.iriseye.common.MouseEmulation;
import com.irisbond.iriseye.interfaces.AccessibilityServiceModeEngine;

import android.accessibilityservice.AccessibilityService;
import android.app.Service;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.view.accessibility.AccessibilityEvent;

/**
 * Engine implementation for the accessibility service which provides
 * a mouse emulation motion processor
 */
public class AccessibilityServiceModeEngineImpl extends CoreEngine
        implements AccessibilityServiceModeEngine {

    // object which decides what to do with a new click
    private ClickDispatcher mClickDispatcher;

    // mouse emulation subsystem
    private MouseEmulation mMouseEmulation;


    @Override
    protected void onInit(@NonNull Service service) {
        mClickDispatcher= new ClickDispatcher((AccessibilityService) service, getOverlayView());

        /* mouse emulation subsystem, should be the last one to add visible layers
           so that the pointer is drawn on top of everything else */
        mMouseEmulation = new MouseEmulation(service, getOverlayView(),
                getOrientationManager(), mClickDispatcher);
    }

    @Override
    protected void onCleanup() {
        if (mMouseEmulation != null) {
            mMouseEmulation.cleanup();
            mMouseEmulation = null;
        }

        if (mClickDispatcher!= null) {
            mClickDispatcher.cleanup();
            mClickDispatcher= null;
        }
    }

    @Override
    protected final boolean onStart() {
        mMouseEmulation.start();
        mClickDispatcher.start();

        return true;
    }

    @Override
    protected void onStop() {
        mMouseEmulation.stop();
        mClickDispatcher.stop();
    }

    @Override
    protected void onPause() {
        mMouseEmulation.stop();
        mClickDispatcher.stop();
    }

    @Override
    protected void onStandby() {
        mMouseEmulation.stop();
        mClickDispatcher.stop();
    }

    @Override
    protected void onResume() {
        onStart();
    }

    /**
     * Called for each processed camera frame
     *
     * @param motion motion vector, could be (0, 0) if motion not detected or the engine is
     *               paused or in standby mode
     * @param faceDetected whether or not a face was detected for the last frame, note
     *                     not all frames are checked for the face detection algorithm
     * @param state current state of the engine
     *
     * NOTE: called from a secondary thread
     */
    @Override
    protected void onFrame(@NonNull PointF motion, boolean faceDetected, int state) {
        if (state == STATE_RUNNING) {
        }
        try {
            mMouseEmulation.processMotion(motion);
            mClickDispatcher.refresh();
            mMouseEmulation.setRestMode(mClickDispatcher.getRestModeEnabled());
        } catch (Exception e) {

        }
    }

    @Override
    public void enablePointer() {
        if (mMouseEmulation != null) mMouseEmulation.enablePointer();
    }

    @Override
    public void disablePointer() {
        if (mMouseEmulation != null) mMouseEmulation.disablePointer();
        if (mClickDispatcher!= null) mClickDispatcher.reset();
    }

    @Override
    public void enableClick() {
        if (mMouseEmulation != null) mMouseEmulation.enableClick();
    }

    @Override
    public void disableClick() {
        if (mMouseEmulation != null) mMouseEmulation.disableClick();
        if (mClickDispatcher!= null) mClickDispatcher.reset();
    }

    @Override
    public void enableDockPanel() {
        if (mClickDispatcher!= null) mClickDispatcher.enableDockPanel();
    }

    @Override
    public void disableDockPanel() {
        if (mClickDispatcher!= null) mClickDispatcher.disableDockPanel();
    }

    @Override
    public void enableScrollButtons() {
        if (mClickDispatcher!= null) mClickDispatcher.enableScrollButtons();
    }

    @Override
    public void disableScrollButtons() {
        if (mClickDispatcher!= null) mClickDispatcher.disableScrollButtons();
    }

    @Override
    public void enableAll() {
        enablePointer();
        enableClick();
        enableDockPanel();
        enableScrollButtons();
    }

    @Override
    public void onAccessibilityEvent(@NonNull AccessibilityEvent event) {
        if (mClickDispatcher != null) {
            mClickDispatcher.onAccessibilityEvent(event);
        }
    }

    @Override
    public long getFaceDetectionElapsedTime() {
        return 0;
    }

    @Override
    public void updateFaceDetectorStatus(FaceDetectionCountdown fdc) {

    }
}
