

package com.irisbond.iriseye.service;

import android.accessibilityservice.AccessibilityService;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;

import com.irisbond.iriseye.BuildConfig;
import com.irisbond.iriseye.EngineSelector;
import com.irisbond.iriseye.R;
import com.irisbond.iriseye.activity.LauncherActivity;
import com.irisbond.iriseye.common.Analytics;
import com.irisbond.iriseye.common.CrashRegister;
import com.irisbond.iriseye.interfaces.AccessibilityServiceModeEngine;
import com.irisbond.iriseye.interfaces.Engine;
import com.irisbond.iriseye.common.Preferences;
import com.irisbond.iriseye.util.GlobalSettings;


/**
 * The Enable Viacam accessibility service
 */
public class IrisbondAccessibilityService extends AccessibilityService
        implements ComponentCallbacks, Engine.OnInitListener {

    private static final String TAG = "IrisbondAcesibilitySrv";

    private static IrisbondAccessibilityService sIrisbondAccessibilityService;

    // reference to the engine
    private AccessibilityServiceModeEngine mEngine;

    // stores whether the accessibility service was previously started (see comments on init())
    private boolean mServiceStarted = false;

    // reference to the notification management stuff
    private ServiceNotification mServiceNotification;



    private int mDeviceOrientation;
    private int mDeviceRotation;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;

    // Receiver listener for the service notification
    private final BroadcastReceiver mServiceNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            int action = intent.getIntExtra(ServiceNotification.NOTIFICATION_ACTION_NAME, -1);

            if (action == ServiceNotification.NOTIFICATION_ACTION_STOP) {
                /* Ask for confirmation before stopping */
                AlertDialog ad = new AlertDialog.Builder(c)
                    .setMessage(c.getResources().getString(R.string.notification_stop_confirmation))
                    .setPositiveButton(c.getResources().getString(
                            R.string.notification_stop_confirmation_no), null)
                    .setNegativeButton(c.getResources().getString(
                            R.string.notification_stop_confirmation_yes),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    cleanupEngine();
                                    Preferences.get().setEngineWasRunning(false);
                                }
                            })
                   .create();
                //noinspection ConstantConditions
                int LAYOUT_FLAG;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
                } else {
                    LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
                }
                ad.getWindow().setType(LAYOUT_FLAG);
                ad.show();
            } else if (action == ServiceNotification.NOTIFICATION_ACTION_START) {
                initEngine();
            } else {
                // ignore intent
                Log.i(TAG, "mServiceNotificationReceiver: Got unknown intent");
            }
        }
    };

    /**
     * Start the initialization sequence of the accessibility service.
     */
    private void init() {
        /*
         * Check if service has been already started.
         * Under certain circumstances onUnbind is not called (e.g. running
         * on an emulator happens quite often) and the service continues running
         * although it shows it is disabled. This does not solve the issue but at
         * least the service does not crash
         *
         * http://stackoverflow.com/questions/28752238/accessibilityservice-onunbind-not-always-
         * called-when-running-on-emulator
         */
        if (mServiceStarted) {
            Log.w(TAG, "Accessibility service already running! Stop here.");

            return;
        }

        mServiceStarted= true;
        sIrisbondAccessibilityService = this;

         /* When preferences are not properly initialized (i.e. is in slave mode)
           the call will return null. As is not possible to stop the accessibility
           service just take into account an avoid further actions. */
        if (Preferences.initForA11yService(this) == null) return;

        // Service notification
        mServiceNotification= new ServiceNotification(this, mServiceNotificationReceiver);
        mServiceNotification.init();

        /*
         * If crashed recently, abort initialization to avoid several crash messages in a row.
         * The user will need to enable it again through the notification icon.
         */
        if (CrashRegister.crashedRecently(this)) {
            Log.w(TAG, "Recent crash detected. Aborting initialization.");
            CrashRegister.clearCrash(this);
            mServiceNotification.update(ServiceNotification.NOTIFICATION_ACTION_START);
            return;
        }

        /*
         * If the device has been rebooted and the engine was stopped before
         * such a reboot, do not start.
         */
        if (!Preferences.get().getEngineWasRunning()) {
            mServiceNotification.update(ServiceNotification.NOTIFICATION_ACTION_START);
            return;
        }

        initEngine();
    }

    /**
     * Cleanup accessibility service before exiting completely
     */
    private void cleanup() {
        sIrisbondAccessibilityService = null;

        cleanupEngine();

        if (Preferences.get() != null) {
            Preferences.get().cleanup();
        }

        if (mServiceNotification!= null) {
            mServiceNotification.cleanup();
            mServiceNotification= null;
        }

        mServiceStarted = false;
    }

    /**
     * Start engine initialization sequence. When finished, onInit is called
     */
    private void initEngine() {
        if (null != mEngine) {
            Log.i(TAG, "Engine already initialized. Ignoring.");
            return;
        }

        // During initialization cannot send new commands
        mServiceNotification.update(ServiceNotification.NOTIFICATION_ACTION_NONE);

        /* Init the main engine */
        mEngine = EngineSelector.initAccessibilityServiceModeEngine();
        if (mEngine == null) {
            Log.e(TAG, "Cannot initialize CoreEngine in A11Y mode");
        } else {
            mEngine.init(this, this);
        }
    }

    /**
     * Callback for engine initialization completion
     * @param status 0 if initialization completed successfully
     */
    @Override
    public void onInit(int status) {
        if (status == Engine.OnInitListener.INIT_SUCCESS) {

            initEnginePhase2();
        }
        else {
            // Initialization failed
            Log.e(TAG, "Cannot initialize CoreEngine in A11Y mode");
            cleanupEngine();
            mServiceNotification.update(ServiceNotification.NOTIFICATION_ACTION_START);
        }
    }
    private void getDeviceOrientation() {
        int orientation = getResources().getConfiguration().orientation;

        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.i(TAG,"Configuration.ORIENTATION_LANDSCAPE");
            mDeviceOrientation = 2;

        } else if(orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.i(TAG, "Configuration.ORIENTATION_PORTRAIT");
            mDeviceOrientation = 1;
        }
    }

    private void getDeviceRotation() {
        mDeviceRotation = computeDeviceRotation();
    }

    private int computeDeviceRotation() {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        int rotation = windowManager.getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return 0;
            case Surface.ROTATION_90:
                return 90;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_270:
                return 270;
        }
        return 0;
    }

    private void startService() {

        Log.i(TAG, ">>>> Starting the service...");
        getDeviceOrientation();
        getDeviceRotation();


        //mWebTrackerCore.setup(getApplicationContext().getPackageName(), "43307a95-3aff-4d20-88b7-f7c2db25008c", mDeviceOrientation, mDeviceRotation);

    }
    /**
     * Completes the initialization of the engine
     */
    private void initEnginePhase2() {
        //Analytics.get().trackStartService();

        Preferences.get().setEngineWasRunning(true);



            startService();
            mEngine.start();

          //  mWebTrackerCore.setOnDataAvailableListener(mEngine.getOnDataAvailableListener());


        mServiceNotification.update(ServiceNotification.NOTIFICATION_ACTION_STOP);
    }

    /**
     * Receiver listener for the event triggered when the wizard is finished
     */
    private final BroadcastReceiver mFinishWizardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mEngine!= null) {
                mEngine.start();
              //  mWebTrackerCore.setOnDataAvailableListener(mEngine.getOnDataAvailableListener());
                mServiceNotification.update(ServiceNotification.NOTIFICATION_ACTION_STOP);
            }
            LocalBroadcastManager.getInstance(IrisbondAccessibilityService.this).
                    unregisterReceiver(mFinishWizardReceiver);
        }
    };

    /**
     * Stop the engine and free resources
     */
    private void cleanupEngine() {
        if (null == mEngine) return;

        Analytics.get().trackStopService();

        if (mEngine!= null) {
            mEngine.cleanup();
            mEngine= null;
        }

        EngineSelector.releaseAccessibilityServiceModeEngine();

        mServiceNotification.update(ServiceNotification.NOTIFICATION_ACTION_START);
    }

    /**
     * Get the current instance of the accessibility service
     *
     * @return reference to the accessibility service or null
     */
    public static @Nullable
    IrisbondAccessibilityService get() {
        return sIrisbondAccessibilityService;
    }

    public void openNotifications() {
        performGlobalAction(AccessibilityService.GLOBAL_ACTION_NOTIFICATIONS);
    }

    /**
     * Called when the accessibility service is started
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate");
     //   mWebTrackerCore = WebTrackerCore.getInstance(getApplicationContext());
        Log.i(TAG, ">>>>>>>> Configuring the service...");
        InitServiceTask task = new InitServiceTask();
        task.execute();
      //  mWebTrackerCore.setServiceStatusListener(mServiceStatusListener);

    }

    /**
     * Called every time the service is switched ON
     */
    @Override
    public void onServiceConnected() {
        Log.i(TAG, "onServiceConnected");
        init();
    }

    /**
     * Called when service is switched off
     */
    @Override
    public boolean onUnbind(Intent intent) {
        /* TODO: it seems that, at this point, views are already destroyed
         * which might be related with the spurious crashes when switching
         * off the accessibility service. Tested on Nexus 7 Android 5.1.1
         */
        if (BuildConfig.DEBUG) Log.d(TAG, "onUnbind");
        cleanup();
        return false;
    }

    /**
     * Called when service is switched off after onUnbind
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (BuildConfig.DEBUG) Log.d(TAG, "onDestroy");
        cleanup();
    }

    /**
     * (required) This method is called back by the system when it detects an
     * AccessibilityEvent that matches the event filtering parameters specified
     * by your accessibility service.
     */
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (mEngine != null) {
            mEngine.onAccessibilityEvent(event);
        }
    }

    /**
     * (required) This method is called when the system wants to interrupt the
     * feedback your service is providing, usually in response to a user action
     * such as moving focus to a different control. This method may be called
     * many times over the life cycle of your service.
     */
    @Override
    public void onInterrupt() {
        if (BuildConfig.DEBUG) Log.d(TAG, "onInterrupt");
    }

    private class InitServiceTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
           // int result =mWebTrackerCore.init();
            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            /* Start wizard or the full engine? */
            SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            if (result==0 &&Preferences.get().getRunTutorial() && prefs.getBoolean(GlobalSettings.WIZARD_PASSED,false)) {
                // register notification receiver
                LocalBroadcastManager.getInstance(IrisbondAccessibilityService.this).registerReceiver(
                        IrisbondAccessibilityService.this.mFinishWizardReceiver,
                        new IntentFilter("wizard-closed-event"));

                Intent dialogIntent = new Intent(IrisbondAccessibilityService.this, com.irisbond.iriseye.wizard.WizardActivity.class);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                IrisbondAccessibilityService.this.startActivity(dialogIntent);
            }
        }
    }
}
