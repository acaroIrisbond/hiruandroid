

package com.irisbond.iriseye.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.irisbond.iriseye.R;
import com.irisbond.iriseye.activity.CalibrationActivity;
import com.irisbond.iriseye.activity.MousePreferencesActivity;

/**
 * Manage pause/resume notifications
 */
class ServiceNotification {
    /**
     * Type of notification to display
     */
    static final int NOTIFICATION_ACTION_NONE = 0;
    static final int NOTIFICATION_ACTION_STOP = 1;
    static final int NOTIFICATION_ACTION_START = 2;

    /*
     * constants for notifications
     */
    private static final int NOTIFICATION_ID = 1;
    private static final String NOTIFICATION_FILTER_ACTION = "ENABLE_DISABLE_EVIACAM";
    static final String NOTIFICATION_ACTION_NAME = "action";

    private final Service mService;

    private final BroadcastReceiver mBroadcastReceiver;

    private int mAction= NOTIFICATION_ACTION_NONE;

    private boolean mInitDone = false;

    /**
     * Constructor
     *
     * @param s  service
     * @param bc broadcast receiver which will be called when the notification is tapped
     */
    ServiceNotification(@NonNull Service s, @NonNull BroadcastReceiver bc) {
        mService = s;
        mBroadcastReceiver = bc;
    }

    public void init() {
        if (mInitDone) return;

        /* register notification receiver */
        IntentFilter iFilter = new IntentFilter(NOTIFICATION_FILTER_ACTION);
        mService.registerReceiver(mBroadcastReceiver, iFilter);

        updateNotification();

        mInitDone = true;
    }

    public void cleanup() {
        if (!mInitDone) return;

        // Remove as foreground service
        mService.stopForeground(true);

        // Remove notification
        NotificationManager notificationManager =
                (NotificationManager) mService.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);

        mService.unregisterReceiver(mBroadcastReceiver);

        mInitDone = false;
    }

    /**
     * Create and register the notification as foreground service
     */
    private void updateNotification () {

        Notification noti = createNotification(mService, mAction);
        NotificationManager notificationManager =
                (NotificationManager) mService.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, noti);

        // Register as foreground service
        mService.startForeground(ServiceNotification.NOTIFICATION_ID, noti);
    }


    /**
     * Set the action of the notification and update accordingly
     * @param action of the notification
     *             NOTIFICATION_ACTION_NONE
     *             NOTIFICATION_ACTION_STOP
     *             NOTIFICATION_ACTION_START
     */
    void update (int action) {
        if (mAction == action) return;
        mAction= action;

        updateNotification ();
    }

    /**
     * Create the notification
     * @param c context
     * @param action code of the action
     * @return return notification object
     */
    private static Notification createNotification(@NonNull Context c, int action) {
        Intent intent;

        /* Pending intent to notify the a11y service */
        intent = new Intent(NOTIFICATION_FILTER_ACTION);
        intent.putExtra(NOTIFICATION_ACTION_NAME, action);
        PendingIntent pIntentAction = PendingIntent.getBroadcast
                (c, NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        /* Pending intent to open settings activity */
        intent = new Intent(c, CalibrationActivity.class);
        PendingIntent pOpenSettings= PendingIntent.getActivity(c, 0, intent, 0);
        Intent calibrationIntent=new Intent(NOTIFICATION_FILTER_ACTION);
        calibrationIntent.putExtra(NOTIFICATION_ACTION_NAME, action);
        PendingIntent pCalibrationIntentAction = PendingIntent.getBroadcast
                (c, NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        /* Choose right text message */
        CharSequence text;
        int iconId= R.drawable.ic_notification_enabled;
        if (action == NOTIFICATION_ACTION_NONE) {
            text = c.getText(R.string.app_name);
        } else if (action == NOTIFICATION_ACTION_STOP) {
            text = c.getText(R.string.notification_running_click_to_stop);
        } else if (action == NOTIFICATION_ACTION_START) {
            text = c.getText(R.string.notification_stopped_click_to_start);
        }
        else throw new IllegalStateException();
        String channel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            channel = createChannel(c);
        else {
            channel = "";
        }
        NotificationCompat.Builder builder= new NotificationCompat.Builder(c,channel)
            .setSmallIcon(iconId)
            .setLargeIcon(BitmapFactory.decodeResource(c.getResources(), R.drawable.launcher_ic))
            .setContentTitle(c.getText(R.string.app_name))
            .setContentText(text)
            .setContentIntent(pIntentAction)
            .setPriority(Notification.PRIORITY_MAX)
            .setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(text))
            .addAction(android.R.drawable.ic_menu_preferences,
                    c.getResources().getString(R.string.notification_settings_label), pOpenSettings)
         .addAction(R.drawable.phone_facing,
                c.getResources().getString(R.string.notification_calibration_label), pOpenSettings);
        return builder.build();
    }

    @NonNull
    @TargetApi(26)
    private static String createChannel( Context c) {
        NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);

        String name = c.getString(R.string.app_name);
        int importance = NotificationManager.IMPORTANCE_LOW;

        NotificationChannel mChannel = new NotificationChannel("snap map channel", name, importance);

        mChannel.enableLights(true);
        mChannel.setLightColor(Color.BLUE);
        if (mNotificationManager != null) {
            mNotificationManager.createNotificationChannel(mChannel);
        }
        return "snap map channel";
    }
}
