package com.irisbond.iriseye.util;

import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import com.irisbond.iriseye.IrisbondCalibrationCommunication.IrisbondCalibration;

public class ProtoMessageManager {

    int screenWidth;
    int screenHeight;

    public ProtoMessageManager(int screenWidth, int screenHeight) {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    public void setScreenSize(int screenWidth, int screenHeight)
    {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    // MARK: - SEND

    public IrisbondCalibration.Message CreateMessage (int messageId, IrisbondCalibration.msg_data msgData)
    {
        IrisbondCalibration.Message message=null;



        switch (messageId) {
        case IrisbondCalibration.message_identifier
                .no_message_VALUE:
        break;

        case IrisbondCalibration.message_identifier.calibration_start_VALUE:

            IrisbondCalibration.msg_calibration_start calibration_startData= IrisbondCalibration.msg_calibration_start.newBuilder()
                    .setNumCalibPoints(5)
                    .setScreenHeight(screenHeight)
                    .setScreenWidth(screenWidth)
                    .build();
            message= IrisbondCalibration.Message.newBuilder()
                    .setMessageId(IrisbondCalibration.message_identifier.calibration_start)
                    .setData(calibration_startData.toByteString())
                    .setDataSize(calibration_startData.toByteString().size())
                    .build();


        break;

        case IrisbondCalibration.message_identifier.gui_started_VALUE:
            IrisbondCalibration.msg_gui_started msgGuiStarted= IrisbondCalibration.msg_gui_started.getDefaultInstance();
            message= IrisbondCalibration.Message.newBuilder()
                    .setData(msgGuiStarted.toByteString())
                    .setDataSize(msgGuiStarted.toByteString().size())
                    .setMessageId(IrisbondCalibration.message_identifier.gui_started)
                    .build();
        break;

        case IrisbondCalibration.message_identifier.point_ready_VALUE:
            IrisbondCalibration.msg_point_ready msgPointReady= IrisbondCalibration.msg_point_ready.getDefaultInstance();
            message= IrisbondCalibration.Message.newBuilder()
                    .setData(msgPointReady.toByteString())
                    .setDataSize(msgPointReady.toByteString().size())
                    .setMessageId(IrisbondCalibration.message_identifier.point_ready)
                    .build();

        break;

        case IrisbondCalibration.message_identifier.coords_received_VALUE:
            IrisbondCalibration.msg_coords_received msgCoordsReceived= IrisbondCalibration.msg_coords_received.getDefaultInstance();
            message= IrisbondCalibration.Message.newBuilder()
                    .setData(msgCoordsReceived.toByteString())
                    .setDataSize(msgCoordsReceived.toByteString().size())
                    .setMessageId(IrisbondCalibration.message_identifier.coords_received)
                    .build();

        break;
        case IrisbondCalibration.message_identifier.calibration_cancel_VALUE:
            IrisbondCalibration.msg_calibration_cancel msgCalibrationCancel= IrisbondCalibration.msg_calibration_cancel.getDefaultInstance();
            message= IrisbondCalibration.Message.newBuilder()
                    .setData(msgCalibrationCancel.toByteString())
                    .setDataSize(msgCalibrationCancel.toByteString().size())
                    .setMessageId(IrisbondCalibration.message_identifier.calibration_cancel)
                    .build();
        break;
        case IrisbondCalibration.message_identifier.calib_cancelled_VALUE:
            IrisbondCalibration.msg_calib_cancelled msgCalibCancelled= IrisbondCalibration.msg_calib_cancelled.getDefaultInstance();
            message= IrisbondCalibration.Message.newBuilder()
                    .setData(msgCalibCancelled.toByteString())
                    .setDataSize(msgCalibCancelled.toByteString().size())
                    .setMessageId(IrisbondCalibration.message_identifier.calib_cancelled)
                    .build();
        break;

            case -1:
        break;
        default:
        break;
    }


      return message;
    }


    // MARK: - RECEIVE

   public IrisbondCalibration.Message readMessage (byte[] serializedData)
    {
        try {
            return IrisbondCalibration.Message.parseFrom(serializedData);
        }
        catch(Exception e) { return null;}
    }

   public GeneratedMessageV3 readData( IrisbondCalibration.Message message) {
        switch (message.getMessageId().getNumber()) {
            case IrisbondCalibration.message_identifier.no_message_VALUE:
                return null;
            case IrisbondCalibration.message_identifier.calibration_start_VALUE:
                try {
                    return IrisbondCalibration.msg_calibration_start.parseFrom(message.getData());
                } catch (Exception e) {
                    return null;
                }
            case IrisbondCalibration.message_identifier.gui_started_VALUE:
                return null;
            case IrisbondCalibration.message_identifier.point_ready_VALUE:
                return null;
            case IrisbondCalibration.message_identifier.coords_received_VALUE:
                return null;
            case IrisbondCalibration.message_identifier.calibration_cancel_VALUE:
                return null;
            case IrisbondCalibration.message_identifier.ok_start_calib_VALUE:
                return null;
            case IrisbondCalibration.message_identifier.new_calib_coords_VALUE:
                try {
                    return IrisbondCalibration.msg_new_calib_coords.parseFrom(message.getData());
                } catch (Exception e) {
                    return null;
                }
            case IrisbondCalibration.message_identifier.calib_cancelled_VALUE:
                return null;
            case IrisbondCalibration.message_identifier.calib_finished_VALUE:
                return null;
            case -1:
                return null;
            case IrisbondCalibration.message_identifier.data_VALUE:
                try {
                    IrisbondCalibration.msg_data data = IrisbondCalibration.msg_data.parseFrom(message.getData());
                    data.setMouseY_(Float.valueOf(screenHeight) - data.getMouseY());
                    data.setMouseRawY_(Float.valueOf(screenHeight) - data.getMouseRawY());
                    data.setLeftEyeX_(Float.valueOf(screenWidth) - data.getLeftEyeX());
                    data.setRightEyeX_(Float.valueOf(screenWidth) - data.getRightEyeX());
                    return data;
                } catch (Exception e) {
                    return null;
                }
            case IrisbondCalibration.message_identifier.image_streaming_VALUE:
                return null;


        }
        return null;
    }

}
