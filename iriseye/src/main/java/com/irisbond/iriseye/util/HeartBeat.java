
package com.irisbond.iriseye.util;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

/**
 * Intermittent toast message
 */
public class HeartBeat {
    private final Timer mTimer;
    private final Handler mToastHandler;

    /* Declare handler as static inner class to avoid memory leaks. See:
        http://www.androiddesignpatterns.com/2013/01/inner-class-handler-memory-leak.html
     */
    private static class HeartBeatHandler extends Handler {
        private final WeakReference<Context> mContext;
        private final String mMessage;

        HeartBeatHandler(Context c, String msg) {
            mContext= new WeakReference<>(c);
            mMessage= msg;
        }

        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(mContext.get(), mMessage, Toast.LENGTH_SHORT).show();
        }
    }

    public HeartBeat(final Context context, String msg) {
        mTimer= new Timer();
        mToastHandler = new HeartBeatHandler(context, msg);
    }
    
    public void start() {
        mTimer.scheduleAtFixedRate(new mainTask(), 0, 5000);
    }
    
    public void stop() {
        mTimer.cancel();
    }
    
    private class mainTask extends TimerTask {
        public void run() {
            mToastHandler.sendEmptyMessage(0);
        }
    }
}
