package com.irisbond.iriseye.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.view.SurfaceView;
import android.view.SurfaceHolder;

/**
 * Created by zhaosong on 2018/6/16.
 */

public class CustomSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private final int width;
    private final int height;
    private SurfaceHolder surfaceHolder = null;



    public CustomSurfaceView(Context context, int width, int height) {
        super(context);
        this.width=width;
        this.height=height;
        setFocusable(true);

        if(surfaceHolder == null) {
            // Get surfaceHolder object.
            surfaceHolder = getHolder();
            // Add this as surfaceHolder callback object. 
            surfaceHolder.addCallback(this);
        }




        // Set current surfaceview at top of the view tree.
        this.setZOrderOnTop(true);

        this.getHolder().setFormat(PixelFormat.TRANSLUCENT);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }




    public void drawEyes(Point[] coordsNorm, boolean[] detection , float distance) throws Exception {

        Canvas canvas = surfaceHolder.lockCanvas();
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        Paint surfaceBackground = new Paint();


        float elipseSize = (width/4) * 0.10f;
        float leftX = (coordsNorm[0].x) - (elipseSize / 2);
        float leftY = (coordsNorm[0].y) - (elipseSize / 2);
        float rightX = (coordsNorm[1].x) - (elipseSize / 2);
        float rightY = (coordsNorm[1].y) - (elipseSize / 2);


        int color;

        if (distance >= -0.7 && distance <= 1.2) {
            //               fillColor = UIColor.green.cgColor
            color = Color.GREEN;

        } else {
            //                fillColor = UIColor.red.cgColor
            color = Color.RED;
        }

        surfaceBackground.setColor(color);


        if (detection[0]) {
            canvas.drawCircle(leftX, leftY, elipseSize, surfaceBackground);
        }
        if (detection[1]) {
            canvas.drawCircle(rightX, rightY, elipseSize, surfaceBackground);
        }

        surfaceHolder.unlockCanvasAndPost(canvas);
    }


}