
package com.irisbond.iriseye.util;

/**
 * Implements a time based blinker with equal duration for the ON/OFF states
 */
public class Blinker {
    // Duration of the ON/OFF state
    private final long mDuration;

    // Time when the blinker was been started (0 means stopped)
    private long mStartTime= 0;

    /**
     * Constructor
     *
     * @param duration duration of the blinker
     */
    public Blinker(int duration) {
        mDuration = duration;
    }

    /**
     * Start the blinker. Do nothing if already started.
     */
    public void start() {
        if (mStartTime!= 0) return;
        mStartTime= System.currentTimeMillis();
    }

    /**
     * Stop the blinker
     */
    public void stop() {
        mStartTime= 0;
    }

    /**
     * Get the current state of the blinker
     *
     * @return true means ON state, false means OFF or disabled
     */
    public boolean getState() {
        return mStartTime == 0 || (((System.currentTimeMillis() - mStartTime) / mDuration) & 1)== 0;
    }
}
