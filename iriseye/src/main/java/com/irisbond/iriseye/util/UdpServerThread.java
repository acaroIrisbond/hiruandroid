package com.irisbond.iriseye.util;

import android.util.Log;

import com.irisbond.iriseye.IrisbondCalibrationCommunication.IrisbondCalibration;
import com.irisbond.iriseye.interfaces.OnMessageReceived;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Date;

public class UdpServerThread extends Thread{
        private static final String TAG= "UDP_server";
        int serverPort=8082;
        DatagramSocket socket;
        OnMessageReceived mListener;
        int bufferMaxSize=4096;

        boolean running;

        public UdpServerThread(int serverPort, int bufferMaxSize,OnMessageReceived listener) {
            super();
            this.serverPort = serverPort;
            this.bufferMaxSize=bufferMaxSize;
            this.mListener=listener;

        }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running){
            this.running = running;
        }




    @Override
        public void run() {

            running = true;

            try {
                Log.v(TAG, "Starting UDP Server");
                socket = new DatagramSocket(serverPort);

                Log.v(TAG, "UDP Server is running");

                while(running){
                    byte[] buf = new byte[bufferMaxSize];

                    // receive request
                    DatagramPacket packet = new DatagramPacket(buf, buf.length);

                    socket.receive(packet);
                   byte[] databuffer= Arrays.copyOfRange(packet.getData(), 0, packet.getLength()); ;

                    mListener.messageReceived(IrisbondCalibration.Message.parseFrom(databuffer));
                    int port = packet.getPort();
                    InetAddress address = packet.getAddress();
                    Log.d(TAG, "Request from: " + address + ":" + port + "\n");


                }

                Log.v(TAG, "UDP Server ended");

            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(socket != null){
                    socket.close();
                    Log.v(TAG, "socket.close()");
                }
            }
        }
    //Declare the interface. The method messageReceived(String message) will must be implemented in the Activity
    //class at on AsyncTask doInBackground
        public void disconnect(){
            running=false;
            socket.close();
        }

}

