
package com.irisbond.iriseye.util;

import java.util.List;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;

import com.irisbond.iriseye.common.IRISEYE;

/**
 * Debugging code for AccessibilityWindowInfo (API +21)
 */
@SuppressWarnings("unused")
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class AccessibilityWindowDebug {
    static
    public void displayFullWindowTree (List<AccessibilityWindowInfo> l) {
        Log.d(IRISEYE.TAG, "Accessibility window tree dump:");
        int i= 1;
                
        for (AccessibilityWindowInfo w : l) {
            displayFullWindowTree0(w, "W" + Integer.toString(i++));
        }
    }
    
    static private void displayFullWindowTree0 (AccessibilityWindowInfo win, String prefix) {
        Log.d(IRISEYE.TAG, prefix + " " + win.toString());

        // has nodes?
        AccessibilityNodeInfo node= win.getRoot();
        if (node != null) {
            AccessibilityNodeDebug.displayFullTree0 (node, prefix + ".1");
        }
        
        // propagate calls to children
        for (int i= 0; i< win.getChildCount(); i++) {
            String newPrefix= " " + prefix + "." + Integer.toString(i + 1);
            displayFullWindowTree0(win.getChild(i), newPrefix);
        }
    }
}
