package com.irisbond.iriseye.util;

import android.util.Log;

import com.google.protobuf.InvalidProtocolBufferException;
import com.irisbond.iriseye.IrisbondCalibrationCommunication.IrisbondCalibration;
import com.irisbond.iriseye.interfaces.OnMessageReceived;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

public class TcpClient {

    public static final String TAG = TcpClient.class.getSimpleName();
    //public   String SERVER_IP = "172.16.83.24"; //server IP address
    public   String SERVER_IP = "192.168.1.4"; //server IP address

    public   int SERVER_PORT = 8081;
    public int bufferMaxSize= 512;
    private OnMessageReceived mMessageListener = null;
    private boolean mRun = false;
    private OutputStream outputStream;
    public TcpClient(OnMessageReceived listener, String server_ip, int server_port, int bufferMaxSize) {
        SERVER_IP= server_ip;
        SERVER_PORT= server_port;
        mMessageListener = listener;
        this.bufferMaxSize=bufferMaxSize;
    }


    public void sendMessage(final IrisbondCalibration.Message message) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (outputStream != null) {

                    try {
                        message.writeTo(outputStream);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    public void stopClient() throws IOException {

        mRun = false;


        if(outputStream!=null){
            outputStream.close();
        }
    }

    public void run() {

        mRun = true;

        try {
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            Log.d("TCP Client", "C: Connecting...");

            Socket socket = new Socket(serverAddr, SERVER_PORT);

            try {

                outputStream = socket.getOutputStream();

                while (mRun) {
                    InputStream stream = socket.getInputStream();
                    byte[] data = new byte[bufferMaxSize];
                    int count = stream.read(data);

                    byte[] buffer = Arrays.copyOfRange(data, 0, count);

                    if (count>0) {

                        try {
                            mMessageListener.messageReceived(IrisbondCalibration.Message.parseFrom(buffer));
                        } catch (InvalidProtocolBufferException e) {
                            Log.e("TCP", "S: Error", e);
                        }
                    }



                }

            } catch (Exception e) {
                Log.e("TCP", "S: Error", e);
            } finally {
                socket.close();
            }

        } catch (Exception e) {
            Log.e("TCP", "C: Error", e);
        }




    }

    public boolean ismRun() {
        return mRun;
    }




}