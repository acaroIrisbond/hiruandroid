package com.irisbond.iriseye.util;

public class HiruConfig {

    public static final float kTravelTime = 1.0f;
    public static final float kFixationime  = 1.5f;


    public static final int kTCPBufferMaxSize = 512;
    public static final int kUDPBufferMaxSize = 4096;

    //public static final String kHost = "192.168.0.22";
    public static final String kHost = "192.168.0.4";

   // public static final String kHost = "172.16.83.24";
    //public static final String kHost = "192.168.83.24";
    public static final int kCalibrationPort  = 8081;
    public static final int kDataPort  = 8082;
}
