package com.irisbond.iriseye.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

public class CustomView extends View {
    OnCalibrationAnimFinish animFinishListener;
    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public OnCalibrationAnimFinish getAnimFinishListener() {
        return animFinishListener;
    }

    public void setAnimFinishListener(OnCalibrationAnimFinish animFinishListener) {
        this.animFinishListener = animFinishListener;
    }

    public void move(int coordX , int coordY, float travelingTime)
    {

        int width = this.getWidth();
        int height = this.getHeight();
        int  left = this.getLeft();
        int top = this.getTop();

        int originX = coordX - Integer.valueOf(width / 2);
        int originY = coordY - Integer.valueOf(height / 2);

        int variationX = originX - Integer.valueOf(left);
        int variationY = originY - Integer.valueOf(top);
        Log.v("CustomView","calibration anim x:"+variationX+" y:"+variationY);
        ObjectAnimator animationX = ObjectAnimator.ofFloat(this, "x", originX);
        ObjectAnimator animationY = ObjectAnimator.ofFloat(this, "y", originY);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animationX, animationY);
        animSetXY.start();
        animSetXY.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                         if(animFinishListener!=null)animFinishListener.onAnimEnd();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });



    }

    public void fixation(float fixationTime)
    {
        final long interval2   = (long)(fixationTime * 0.5);
        final long interval1 = (long)(fixationTime) - interval2;

        final Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                1.6f, 1.6f, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(interval1);
        startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation anim= new ScaleAnimation( 1f, 1f, // Start and end values for the X axis scaling
                        1.0f, 1.0f, // Start and end values for the Y axis scaling
                        Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                        Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
                anim.setFillAfter(true); // Needed to keep the result of the animation
                anim.setDuration(interval2);
                startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if(animFinishListener!=null)animFinishListener.onAnimEnd();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });



    }

        public interface OnCalibrationAnimFinish{
                void onAnimEnd();
        }

}
