package com.irisbond.iriseye.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by laurentzi on 16/05/18.
 *
 * Global settings, preferences names, and static variables commons for all app classes
 */

public class GlobalSettings {

    public static final String WIZARD_PASSED="WIZARD_PASSED";
    public static final String CAMERA_DISTANCE_TO_BORDER="CAMERA_DISTANCE_TO_BORDER";
    public static final String CALIBRATION_DONE="CALIBRATION_DONE";
    public static final String MINIMALS_PASSED ="MINIMALS_PASSED" ;
    public static final String SETTINGS_DISCOVERY_MOTION_LAUNCHED ="SETTINGS_DISCOVERY_MOTION_LAUNCHED" ;
    public static final String SETTINGS_DISCOVERY_TIMELINE_LAUNCHED ="SETTINGS_DISCOVERY_TIMELINE_LAUNCHED" ;
    public static final String SETTINGS_DISCOVERY_WRITE_TWEET_LAUNCHED ="SETTINGS_DISCOVERY_WRITE_TWEET_LAUNCHED" ;
    public static final String SETTINGS_DISCOVERY_DISPLAY_TWEET_LAUNCHED ="SETTINGS_DISCOVERY_DISPLAY_TWEET_LAUNCHED" ;
    public static final String CUSTOM_HASHTAGS = " #tuitsaojo #TATGranada2018 #irisbond";

    public static final long DISCOVERY_TIME_LAPSE = 6500;

    public static boolean ENABLE_DISCOVERY_MOTION_MANAGER_WITHOUT_PREFERENCE=true;


    public static void clearWizardAndDiscoveryPreferences(Context context) {
        SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().remove(SETTINGS_DISCOVERY_MOTION_LAUNCHED).commit();
        prefs.edit().remove(SETTINGS_DISCOVERY_TIMELINE_LAUNCHED).commit();
        prefs.edit().remove(SETTINGS_DISCOVERY_WRITE_TWEET_LAUNCHED).commit();
        prefs.edit().remove(SETTINGS_DISCOVERY_DISPLAY_TWEET_LAUNCHED).commit();
        prefs.edit().remove(WIZARD_PASSED).commit();
        prefs.edit().remove(CALIBRATION_DONE).commit();
    }
}
