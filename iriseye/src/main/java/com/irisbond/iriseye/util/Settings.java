package com.irisbond.iriseye.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;

import com.irisbond.iriseye.R;


@SuppressWarnings("WeakerAccess")
public class Settings {

    /**
     * Preference keys
     */
    public static final String KEY_DWELL_TIME= "dwell_time";
    public static final String KEY_DWELL_AREA= "dwell_area";
    public static final String KEY_SOUND_ON_CLICK= "sound_on_click";
    public static final String KEY_CONSECUTIVE_CLICKS = "consecutive_clicks";
    public static final String KEY_DOCKING_PANEL_EDGE= "docking_panel_edge";
    public static final String KEY_UI_ELEMENTS_SIZE= "ui_elements_size";
    private static final String KEY_RUN_TUTORIAL= "run_tutorial";
    public static final String KEY_SHOW_LAUNCHER_HELP= "show_launcher_help";
    public static final String KEY_TUTORIAL_RUNNING= "tutorial_running";
    public static final String KEY_USE_CAMERA2_API= "use_camera2_api";
    public static final String KEY_ENGINE_WAS_RUNNING= "engine_was_running";
    private static final String KEY_SHOW_CONTEXT_MENU_HELP = "display_context_menu_help";

    /**
     * Gamepad locations
     */

    @SuppressWarnings("unused")
    public static final int LOCATION_GAMEPAD_BOTTOM_RIGHT= 5;

    /**
        Run-time constants
     */

    private static  boolean SOUND_ON_CLICK_DEFAULT=false;
    private String[] TIME_WITHOUT_DETECTION_ENTRIES;
    private String[] TIME_WITHOUT_DETECTION_VALUES;

    // singleton instance
    private static Settings sInstance= null;


    // initialization counter, allow for nested initialization/cleanup
    private int mInitCount= 0;

    // init mode
    private static final int INIT_NONE= 0;
    private static final int INIT_SERVICE= 1;
    private int mInitMode= INIT_NONE;

    // constructor
    private Settings(Context c, SharedPreferences sp) {
        sp= sp;

        /*
         * Read run-time constants
         */
        final Resources r= c.getResources();

        SOUND_ON_CLICK_DEFAULT= r.getBoolean(R.bool.sound_on_click_default);

        TIME_WITHOUT_DETECTION_VALUES= r.getStringArray(R.array.time_without_detection_values);
        TIME_WITHOUT_DETECTION_ENTRIES= r.getStringArray(R.array.time_without_detection_entries);

        /* Make sure both arrays have the same size */
        if (TIME_WITHOUT_DETECTION_VALUES.length != TIME_WITHOUT_DETECTION_ENTRIES.length)
            throw new ExceptionInInitializerError();
    }



    /**
     * Init preferences for A11Y service
     *
     * @param c instance to the accessibility service
     * @return reference to the Settings instances or null if cannot be initialised
     */
    public static Settings initService (Context c) {
        if (sInstance!= null) {
            // already initialised
            if (sInstance.mInitMode != INIT_SERVICE) return null;
        }
        else {

            // As accessibility service use the default preferences
            PreferenceManager.setDefaultValues(c, R.xml.preference_fragment, true);
            sInstance = new Settings(c, PreferenceManager.getDefaultSharedPreferences(c));
            sInstance.mInitMode= INIT_SERVICE;
        }

        ++sInstance.mInitCount;

        return sInstance;
    }



   

    /**
     * Constraint a value within a range
     *
     * @param v the value
     * @param min range min value
     * @param max range max value
     * @return constrained value
     */
    private static int constraint (int v, int min, int max) {
        if (v< min) return min;
        if (v> max) return max;
        return v;
    }






    public static boolean getSoundOnClick(SharedPreferences sp) {
        return sp.getBoolean(
                Settings.KEY_SOUND_ON_CLICK, SOUND_ON_CLICK_DEFAULT);
    }

    public static float getUIElementsSize(SharedPreferences sp) {
        return Float.parseFloat(sp.getString(KEY_UI_ELEMENTS_SIZE, null));
    }




    public static boolean getRunTutorial(SharedPreferences sp) {
        return sp.getBoolean(KEY_RUN_TUTORIAL, true);
    }

    public static void setRunTutorial(SharedPreferences sp, boolean value) {
        SharedPreferences.Editor spe= sp.edit();
        spe.putBoolean(KEY_RUN_TUTORIAL, value);
        spe.apply();
    }

    public static boolean getShowLauncherHelp(SharedPreferences sp) {
        return sp.getBoolean(KEY_SHOW_LAUNCHER_HELP, true);
    }

    public static void setShowLauncherHelp(SharedPreferences sp, boolean value) {
        SharedPreferences.Editor spe= sp.edit();
        spe.putBoolean(KEY_SHOW_LAUNCHER_HELP, value);
        spe.apply();
    }

    public void setTutorialRunning(SharedPreferences sp,boolean value) {
        SharedPreferences.Editor spe= sp.edit();
        spe.putBoolean(KEY_TUTORIAL_RUNNING, value);
        spe.apply();
    }

    public static boolean getTutorialRunning(SharedPreferences sp) {
        return sp.getBoolean(KEY_TUTORIAL_RUNNING, false);
    }

    public enum UseCamera2API { NO, YES, AUTO }
    public UseCamera2API getUseCamera2API(SharedPreferences sp) {
        String val= sp.getString(KEY_USE_CAMERA2_API, "auto");
        if (val.equals("no")) return UseCamera2API.NO;
        if (val.equals("yes")) return UseCamera2API.YES;
        return UseCamera2API.AUTO;
    }





    /**
     * Get whether the engine was running (i.e. not stopped through the notification)
     */
    public static boolean getEngineWasRunning(SharedPreferences sp) {
        return sp.getBoolean(Settings.KEY_ENGINE_WAS_RUNNING, true);
    }

    /**
     * Save whether the engine was running
     * @param v value
     */
    public static void setEngineWasRunning(SharedPreferences sp, boolean v) {
        SharedPreferences.Editor spe= sp.edit();
        spe.putBoolean(Settings.KEY_ENGINE_WAS_RUNNING, v);
        spe.apply();
    }

    /**
     * Get whether need to show the context menu help dialog
     *
     * @return true when need to show it
     */
    public static boolean getShowContextMenuHelp(SharedPreferences sp) {
        return sp.getBoolean(Settings.KEY_SHOW_CONTEXT_MENU_HELP, true);
    }

    /**
     * Save whether need to show the context menu help dialog
     * @param v value to save
     */
    public static void setShowContextMenuHelp(SharedPreferences sp, boolean v) {
        SharedPreferences.Editor spe= sp.edit();
        spe.putBoolean(Settings.KEY_SHOW_CONTEXT_MENU_HELP, v);
        spe.apply();
    }
 }
 