
package com.irisbond.iriseye.common;

import com.google.gson.Gson;
import com.google.protobuf.InvalidProtocolBufferException;
import com.irisbond.iriseye.BuildConfig;
import com.irisbond.iriseye.IrisbondCalibrationCommunication.IrisbondCalibration;
import com.irisbond.iriseye.R;
import com.irisbond.iriseye.activity.SplashActivity;
import com.irisbond.iriseye.interfaces.Engine;
import com.irisbond.iriseye.interfaces.OnMessageReceived;
import com.irisbond.iriseye.util.ProtoMessageManager;
import com.irisbond.iriseye.util.TcpClient;
import com.irisbond.iriseye.util.UdpServerThread;
import com.irisbond.iriseye.view.OverlayView;
import com.irisbond.iriseye.util.HiruConfig;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayDeque;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Provides an abstract implementation for the Engine interface. The class is in charge of:
 *
 * - engine initialization and state management
 * - camera and image processing to detect face and track motion
 * - UI: main overlay and camera viewer
 *
 */
public abstract class CoreEngine implements Engine,
        PowerManagement.OnScreenStateChangeListener {
    // stores when the last detection of a face occurred
    private final FaceDetectionCountdown mFaceDetectionCountdown = new FaceDetectionCountdown();

    // handler to run things on the main thread
    private final Handler mHandler= new Handler();

    // power management stuff
    private PowerManagement mPowerManagement;

    /* current engine state */
    private volatile int mCurrentState= STATE_DISABLED;
    private DatagramSocket udpSocket;

    @Override
    public int getState() {
        return mCurrentState;
    }

    // state before switching screen off
    private int mSaveState= -1;

    /* splash screen has been displayed? */
    private boolean mSplashDisplayed = false;

    /* listener to notify when the initialization is done */
    private OnInitListener mOnInitListener;

    /* reference to the service which started the engine */
    private Service mService;

    /* root overlay view */
    private OverlayView mOverlayView;
    protected OverlayView getOverlayView() { return mOverlayView; }

  //  private OnDataAvailableListener mOnDataAvailableListener;


    /* object which encapsulates rotation and orientation logic */
    private OrientationManager mOrientationManager;
    protected OrientationManager getOrientationManager() { return mOrientationManager; }

    /* Last time a face has been detected */
    private volatile long mLastFaceDetectionTimeStamp;

    /* When the engine is wating for the completion of some operation */
    private boolean mWaitState = false;

    /* Store requests when the engine is waiting for the completion of a previous one.
       These stored requests will be executed eventually in the order as arrived.
       This is needed because some operations (for instance, start or stop). */
    private ArrayDeque<Runnable> mPendingRequests= new ArrayDeque<>();

    /* Abstract methods to be implemented by derived classes */

    /**
     * Called just before the initialization is finished
     *
     * @param service service which started the engine
     */
    protected abstract void onInit(Service service);

    /**
     * Called at the beginning of the cleanup sequence
     */
    protected abstract void onCleanup();

    /**
     * Called at the beginning of the start sequence
     *
     * @return should return false when something went wrong to abort start sequence
     */
    protected abstract boolean onStart();

    /**
     * Called at the end of the stop sequence
     */
    protected abstract void onStop();

    /**
     * Called at the end of the pause sequence
     */
    protected abstract void onPause();

    /**
     * Called at the end of the standby sequence
     */
    protected abstract void onStandby();

    /**
     * Called at the beginning of the resume sequence
     */
    protected abstract void onResume();

    /**
     * Called each time a frame is processed and the engine is in one of these states:
     *     STATE_RUNNING, STATE_PAUSED or STATE_STANDBY
     *
     * @param motion motion vector, could be (0, 0) if motion not detected or the engine is
     *               paused or in standby mode
     * @param faceDetected whether or not a face was detected for the last frame, note
     *                     not all frames are checked for the face detection algorithm
     * @param state current state of the engine
     */
    protected abstract void onFrame(@NonNull PointF motion, boolean faceDetected, int state);


    @Override
    public boolean init(@NonNull Service s, @Nullable OnInitListener l) {
        if (mCurrentState != STATE_DISABLED) {
            // Already started, something went wrong
            throw new IllegalStateException();
        }

        mService= s;
        mOnInitListener= l;

        /* Show splash screen if not already shown. The splash screen is also used to
           request the user the required permissions to run this software.
           In the past, it was also used for OpenCV detection and installation.
           The engine initialization waits until the splash finishes. */
        if (mSplashDisplayed) return init2();
        else {
            /* Register receiver for splash finished */
            LocalBroadcastManager.getInstance(s).registerReceiver(
                    onSplashReady,
                    new IntentFilter(SplashActivity.FINISHED_INTENT_FILTER));

            /* Start splash activity */
            Intent dialogIntent = new Intent(mService, SplashActivity.class);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mService.startActivity(dialogIntent);

            return true;
        }
    }

    /* Receiver which is called when the splash activity finishes */
    private BroadcastReceiver onSplashReady= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean status= intent.getBooleanExtra(SplashActivity.KEY_STATUS, false);
            if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "onSplashReady: onReceive: called");

            /* Unregister receiver */
            LocalBroadcastManager.getInstance(mService).unregisterReceiver(onSplashReady);

            if (status) {
                /* Resume initialization */
                mSplashDisplayed = true;
                init2();
            }
            else {
                /* Notify failed initialization */
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mOnInitListener!= null) {
                            mOnInitListener.onInit(OnInitListener.INIT_ERROR);
                        }
                    }
                });
            }
        }
    };

    /**
     * Init phase 2: actual initialization
     */
    // TODO: remove return value
    private boolean init2() {
        mPowerManagement = new PowerManagement(mService, this);
        /*
         * Create UI stuff: root overlay and camera view
         */
        mOverlayView= new OverlayView(mService);
        mOverlayView.setVisibility(View.INVISIBLE);






        // initialize specific motion processor(s)
        onInit(mService);

        mCurrentState= STATE_STOPPED;

        mSaveState= mCurrentState;

        /* Notify successful initialization */
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mOnInitListener!= null) {
                    mOnInitListener.onInit(OnInitListener.INIT_SUCCESS);
                }
            }
        });
        // TODO: remove return value
        return true;
    }

    private boolean isInWaitState() {
        return mWaitState;
    }

    /**
     * Process requests in two steps. First, the request is added to
     * a queue. After that, if the engine is not waiting for the
     * completion of a previous requests, the queue is processed
     * @param request runnable with the request
     */
    protected void processRequest (Runnable request) {
        // Queue request
        mPendingRequests.add(request);
        dispatchRequests();
    }

    /**
     * Dispatch previously queued requests
     */
    private void dispatchRequests() {
        while (mPendingRequests.size()> 0 && !isInWaitState()) {
            Runnable request = mPendingRequests.remove();
            request.run();
        }
    }

    // TODO: remove return value
    @Override
    public boolean start() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.start");
        final Runnable request= new Runnable() {
            @Override
            public void run() {
                doStart();
            }
        };
        processRequest(request);
        return true;
    }
   /* public OnDataAvailableListener getOnDataAvailableListener(){
        return mOnDataAvailableListener;
    }*/
    private void doStart() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.doStart");
        // If not initialized just fail
        if (mCurrentState == STATE_DISABLED) {
            Log.e(IRISEYE.TAG, "Attempt to start DISABLED engine");
            return;
        }

        // If already running just return startup correct
        if (mCurrentState==STATE_RUNNING) {
            Log.i(IRISEYE.TAG, "Attempt to start already running engine");
            return;
        }

        // If paused or in standby, just resume
        if (mCurrentState == STATE_PAUSED || mCurrentState== STATE_STANDBY) {
            resume();
            return;
        }

        /* At this point means that (mCurrentState== STATE_STOPPED) */

        if (!onStart()) {
            Log.e(IRISEYE.TAG, "start.onStart failed");
            return;
        }

        //mFaceDetectionCountdown.start();

        mPowerManagement.lockFullPower();         // Screen always on
        mPowerManagement.setSleepEnabled(true);   // Enable sleep call

        /* show GUI elements */
        mOverlayView.requestLayout();
        mOverlayView.setVisibility(View.VISIBLE);



        ProtoMessageManager messageManager=null;
       if(mService!=null){
           WindowManager wm = (WindowManager) mService.getSystemService(WINDOW_SERVICE);

           DisplayMetrics displayMetrics = new DisplayMetrics();

           wm.getDefaultDisplay().getMetrics(displayMetrics);

           int height = displayMetrics.heightPixels;
           int width = displayMetrics.widthPixels;

           messageManager= new ProtoMessageManager(width,height);
       }

        try {

            final ProtoMessageManager finalMessageManager = messageManager;
            final OnMessageReceived listener = new OnMessageReceived() {
                @Override
                public void messageReceived(IrisbondCalibration.Message message) {
                    if(message.getMessageId().getNumber()==IrisbondCalibration.message_identifier.calibration_start.getNumber()){

                    }else if(message.getMessageId().getNumber()==IrisbondCalibration.message_identifier.coords_received.getNumber()){
                        final IrisbondCalibration.msg_data msgData;


                            msgData = (IrisbondCalibration.msg_data) finalMessageManager.readData(finalMessageManager.readMessage(message.getData().toByteArray()));
                            new Handler(mService.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.v("CoreEngine","point x:"+msgData.getMouseX()+" y:"+msgData.getMouseY());
                                    mMotion.x = msgData.getMouseX();
                                    mMotion.y = msgData.getMouseY();
                                    processFrame();

                                }
                            });



                    }else if(message.getMessageId().getNumber()==IrisbondCalibration.message_identifier.calib_finished.getNumber()){

                    }else if(message.getMessageId().getNumber()==IrisbondCalibration.message_identifier.calib_cancelled.getNumber()){

                    }

                }
            };
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //TcpClient tcpClient= new TcpClient(listener,kHost,kCalibrationPort,kTCPBufferMaxSize); desde el servicio no gestionamos la calibracion
           // tcpClient.run();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    UdpServerThread udpData= new UdpServerThread(HiruConfig.kDataPort,HiruConfig.kUDPBufferMaxSize,listener);
                    udpData.run();
                }
            }).start();


        } catch (Exception e) {
            throw new RuntimeException(e);
        }

/*DEBUG

        try {


            new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] buffer = new byte[32];
                    //  DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    while (true) try {

        Log.v("socket","point");
        final Gson gson = new Gson();
        //  final Point data= gson.fromJson(message, Point.class);


        DatagramSocket sock = null;
        int timeout = 5000;
        long start = System.currentTimeMillis();
        try {

            sock = new DatagramSocket(9876);
            final DatagramPacket pack = new DatagramPacket(new byte[32], 32);

            sock.receive(pack);



            final String received=new String(pack.getData());
            new Handler(mService.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Point dataP= gson.fromJson(received.substring(0,received.indexOf("}")+1), Point.class);
                    Log.v("socket","point "+received);
                    mMotion.x = dataP.x;
                    mMotion.y = dataP.y;
                    processFrame();

                }
            });
        } catch (SocketTimeoutException e) {
            // expected
            Log.e("errorSocket",e.getMessage());
            long delay = System.currentTimeMillis() - start;
            if (Math.abs(delay - timeout) > 1000) {

            }
        } finally {
            if (sock != null) {
                sock.close();
            }
        }


    } catch (IOException e) {Log.e("socketError",e.getMessage());
    }
}
            }).start();
                    } catch (Exception e) {
                    throw new RuntimeException(e);
                    }
 */
        dispatchRequests();
    }



    @Override
    public void pause() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.pause");
        final Runnable request= new Runnable() {
            @Override
            public void run() {
                doPause();
            }
        };
        processRequest(request);
    }

    private void doPause() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.doPause");
        // If not initialized, stopped or already paused, just stop here
        if (mCurrentState == STATE_DISABLED ||
            mCurrentState == STATE_PAUSED   ||
            mCurrentState == STATE_STOPPED) return;

        /*
         * If STATE_RUNNING or STATE_STANDBY
         */

        mPowerManagement.unlockFullPower();

        onPause();

        mCurrentState= STATE_PAUSED;
    }

    @Override
    public void standby() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.standby");
        final Runnable request= new Runnable() {
            @Override
            public void run() {
                doStandby();
            }
        };
        processRequest(request);
    }

    private void doStandby() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.doStandby");
        // If not initialized, stopped or already standby, just stop here
        if (mCurrentState == STATE_DISABLED ||
            mCurrentState == STATE_STANDBY   ||
            mCurrentState == STATE_STOPPED) return;

        /*
         * If STATE_RUNNING or STATE_PAUSED
         */

        mPowerManagement.unlockFullPower();
        mPowerManagement.setSleepEnabled(true);   // Enable sleep call

        String t = String.format(
                mService.getResources().getString(R.string.service_toast_pointer_stopped_toast),
                Preferences.get().getTimeWithoutDetectionEntryValue());
        IRISEYE.LongToast(mService, t);

        onStandby();

        mCurrentState= STATE_STANDBY;
    }

    @Override
    public void resume() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.resume");
        final Runnable request= new Runnable() {
            @Override
            public void run() {
                doResume();
            }
        };
        processRequest(request);
    }

    private void doResume() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.doResume");
        if (mCurrentState != STATE_PAUSED && mCurrentState!= STATE_STANDBY) return;

        onResume();

        //mCamera.setUpdateViewer(true);


        mPowerManagement.lockFullPower();         // Screen always on
        mPowerManagement.setSleepEnabled(true);   // Enable sleep call



        // make sure that UI changes during pause (e.g. docking panel edge) are applied
        mOverlayView.requestLayout();

        mCurrentState= STATE_RUNNING;
    }    

    @Override
    public void stop() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.stop");
        final Runnable request= new Runnable() {
            @Override
            public void run() {
                doStop();
            }
        };
        processRequest(request);
    }

    private void doStop() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.doStop");
        if (mCurrentState == STATE_DISABLED || mCurrentState == STATE_STOPPED) return;


        mOverlayView.setVisibility(View.INVISIBLE);

        mPowerManagement.unlockFullPower();
        mPowerManagement.setSleepEnabled(false);

        onStop();

        // TODO: consider adding STATE_WAIT_STOP
        mCurrentState= STATE_STOPPED;
    }



    @Override
    public void cleanup() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.cleanup");
        if (mCurrentState == STATE_DISABLED) return;

        /* Stop engine immediately and purge pending requests queue */
        doStop();
        mWaitState= false;
        mPendingRequests.clear();

        // Call derived
        onCleanup();


        if(mOrientationManager!=null)mOrientationManager.cleanup();
        mOrientationManager= null;



        mOverlayView.cleanup();
        mOverlayView= null;

        mPowerManagement.cleanup();
        mPowerManagement = null;

        mCurrentState= STATE_DISABLED;


    }



    @Override
    public boolean isReady() {
        return (mCurrentState != STATE_DISABLED);
    }



    /**
     * Called when screen goes ON or OFF
     */
    @Override
    public void onOnScreenStateChange() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.onOnScreenStateChanged");
        final Runnable request= new Runnable() {
            @Override
            public void run() {
                doOnOnScreenStateChange();
            }
        };
        processRequest(request);
    }

    private void doOnOnScreenStateChange() {
        if (BuildConfig.DEBUG) Log.d(IRISEYE.TAG, "CoreEngine.doOnOnScreenStateChanged");
        if (mPowerManagement.getScreenOn()) {
            // Screen switched on
            if (mSaveState == Engine.STATE_RUNNING ||
                    mSaveState == Engine.STATE_STANDBY) start();
            else if (mSaveState == Engine.STATE_PAUSED) {
                start();
                pause();
            }
        }
        else {
            // Screen switched off
            mSaveState= mCurrentState;
            if (mSaveState!= Engine.STATE_STANDBY) stop();
        }
    }


    // avoid creating a new PointF for each frame
    private PointF mMotion= new PointF(0, 0);

    /**
     * Process incoming camera frames (called from a secondary thread)
     *
     *
     */
boolean movementControl=false;
    public void onMouseMove(MouseEvent event) {

        switch (event.direction) {
            case MouseEvent.MOVE_LEFT:
                if(mMotion.x-10<0){
                    mMotion.x -= 0;

                }else{

                    mMotion.x -= 10;
                }
                break;
            case MouseEvent.MOVE_RIGHT:
                mMotion.x += 10;
                break;
            case MouseEvent.MOVE_UP:
                if(mMotion.y-10<0){
                    mMotion.y -= 0;

                }else{

                    mMotion.y -= 10;
                }
                break;
            case MouseEvent.MOVE_DOWN:
                mMotion.y += 10;
                break;
            case MouseEvent.LEFT_CLICK:

                break;

            default:
                break;
        }

        /*
         * control stuff
         */
            processFrame();
        //windowManager.updateViewLayout(cursorView, cursorLayout);
    }
    public void processFrame() {
        // For these states do nothing
      //  if (mCurrentState== STATE_DISABLED || mCurrentState== STATE_STOPPED ||
        //        isInWaitState()) return;

        /*
         * In STATE_RUNNING, STATE_PAUSED or STATE_STANDBY state.
         * Need to check if face detected
         */




        boolean faceDetected=true;//TODO get faceDetected from our eyetracker library

        if (faceDetected) mLastFaceDetectionTimeStamp= System.currentTimeMillis();



        onFrame(mMotion, faceDetected, mCurrentState);

        // States to be managed below: RUNNING, PAUSED, STANDBY

        if (faceDetected) mFaceDetectionCountdown.start();

        if (mCurrentState == STATE_STANDBY) {
            if (faceDetected) {
                // "Awake" from standby state
                mHandler.post(new Runnable() {
                    @Override
                    public void run() { resume(); } }
                );
                /* Yield CPU to the main thread so that it has the opportunity
                 * to run and change the engine state before this thread continue
                 * running.
                 * Remarks: tried Thread.yield() without success
                 */
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) { /* do nothing */ }
            }
            else {
                // In standby reduce CPU cycles by sleeping but only if screen went off
                if (!mPowerManagement.getScreenOn()) mPowerManagement.sleep();
            }
        }
        else if (mCurrentState == STATE_RUNNING) {
            if (mFaceDetectionCountdown.hasFinished() && !mFaceDetectionCountdown.isDisabled()) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        standby();
                    }
                });
            }
        }

        // Nothing more to do (state == Engine.STATE_PAUSED)
        updateFaceDetectorStatus(mFaceDetectionCountdown);
    }
}
