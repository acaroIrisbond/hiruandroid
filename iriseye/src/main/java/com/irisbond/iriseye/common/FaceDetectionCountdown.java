
package com.irisbond.iriseye.common;

import android.content.SharedPreferences;

import com.irisbond.iriseye.util.Countdown;

/**
 * Manages the time elapsed since last face detection.
 *
 * It is basically a countdown with a preference listener.
 *
 * TODO: review this class because is used as a countdown but
 * also to notify face tracking feedback
 */
public class FaceDetectionCountdown extends Countdown
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    /**
     * Constructor
     */
    FaceDetectionCountdown() {
        super(0);

        // preferences
        SharedPreferences sp= Preferences.get().getSharedPreferences();
        sp.registerOnSharedPreferenceChangeListener(this);
        updateSettings();
    }

    public void cleanup() {
        Preferences.get().getSharedPreferences().
                unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        if (key.equals(Preferences.KEY_TIME_WITHOUT_DETECTION)) {
            updateSettings();
        }
    }

    private void updateSettings() {
        setTimeToWait(Preferences.get().getTimeWithoutDetection() * 1000);
    }

    public boolean isDisabled() {
        return getTimeToWait() == 0;
    }
}
