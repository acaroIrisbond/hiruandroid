
package com.irisbond.iriseye.common;

import android.content.Context;
import android.support.annotation.NonNull;

import com.irisbond.iriseye.BuildConfig;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Google analytics stuff
 */
public class Analytics {

    private static final String SERVICE_CATEGORY = "Service";
    private static final String TRACKING_ID = "";

    /* Singleton instance */
    private static Analytics sInstance;

    /*
        Trackers
     */
    private final Tracker mServiceTracker;

    /* Used to compute time between events */
    private long mStartTime;

    /* Create the Analytics instance. Should be called only once */
    public static void init (@NonNull Context c) {
        sInstance= new Analytics(c);
    }

    /* Get the singleton instance. Might return null */
    public static Analytics get() { return sInstance; }

    public void trackStartService() {
        mStartTime= System.currentTimeMillis();
        mServiceTracker.send(new HitBuilders.EventBuilder()
            .setCategory(SERVICE_CATEGORY)
            .setAction("start")
            .setLabel("service")
            .build());
    }

    // Reference: http://stackoverflow.com/questions/30239406/googleanalytics-track-time-between-events
    public void trackStopService() {
        final long elapsed= System.currentTimeMillis() - mStartTime;
        mServiceTracker.send(new HitBuilders.EventBuilder()
                .setCategory(SERVICE_CATEGORY)
                .setAction("stop")
                .setLabel("service")
                .setValue(elapsed / 1000)
                .setCustomMetric(1, elapsed / 1000)
                .build());
    }

    /* Constructor */
    private Analytics(@NonNull Context c) {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(c);
        analytics.setDryRun(false);
        analytics.setLocalDispatchPeriod((BuildConfig.DEBUG ? 15 : 1800));

        /* Uncomment to avoid sending hits to analytics in debug mode */
        //if (BuildConfig.DEBUG) { mAnalytics.setAppOptOut(true); }


        /*
         *  UI auto activity tracker
         */
        Tracker tracker = analytics.newTracker(TRACKING_ID);
        tracker.enableExceptionReporting(false);
        tracker.enableAdvertisingIdCollection(false);
        tracker.enableAutoActivityTracking(true);

        /*
         *  Service tracker
         */
        mServiceTracker = analytics.newTracker(TRACKING_ID);
        mServiceTracker.enableExceptionReporting(true);
        mServiceTracker.enableAdvertisingIdCollection(false);
        mServiceTracker.enableAutoActivityTracking(false);
        /*
        String metricValue = SOME_METRIC_VALUE_SUCH_AS_123_AS_STRING;
        mServiceTracker.set(Fields.customMetric(1), metricValue);
        */
    }
}
