package com.irisbond.iriseye.wizard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.irisbond.iriseye.EngineSelector;
import com.irisbond.iriseye.R;
import com.irisbond.iriseye.activity.LauncherActivity;
import com.irisbond.iriseye.interfaces.AccessibilityServiceModeEngine;
import com.irisbond.iriseye.util.GlobalSettings;


/**
 * Created by laurentzi on 14/05/18.
 */

public class WizardActivity extends AppIntro {
    SharedPreferences prefs;
    private static final int SLIDE_WELLCOME_INDEX=0;
    private static final int SLIDE_VERIFICATION_INDEX=1;
    private static final int SLIDE_CHECKER_INDEX=2;
    private static final int SLIDE_CONFIGURATION_INDEX=3;
    private static final int SLIDE_LOCATOR_INDEX=4;
    private static final int SLIDE_POSITION_INDEX=5;
    private int slidePosition=0;
    private static final int CALIBRATION_ACTIVITY = 3;
    private static final int WIZARD_ACTIVITY =5 ;
    private  WizardCameraLocatorFragment cameraLocatorFragment;
    private String TAG=WizardActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        addSlide(AppIntroFragment.newInstance(getString(R.string.welcome_title), getString(R.string.wizard_wellcome_message), R.drawable.ic_bootsplash, getColor(R.color.colorPrimary)));
        addSlide(AppIntroFragment.newInstance(getString(R.string.wizard_verification_tittle), getString(R.string.wizard_verification_message), R.drawable.validation, getColor(R.color.colorPrimary)));
        WizardCheckMinimalsFragment checkMinimalsFragment= WizardCheckMinimalsFragment.newInstance();
        addSlide(checkMinimalsFragment);
        if(WizardCheckMinimalsFragment.qualityPassed()){

            addSlide(AppIntroFragment.newInstance(getString(R.string.wizard_configuration_tittle), getString(R.string.wizard_configuration_message), R.drawable.settings, getColor(R.color.colorPrimary)));
             cameraLocatorFragment= WizardCameraLocatorFragment.newInstance();
            addSlide(cameraLocatorFragment);
            addSlide(AppIntroFragment.newInstance(getString(R.string.wizard_position_tittle), getString(R.string.wizard_position_message), R.drawable.phone_facing, getColor(R.color.colorPrimary)));
        }


        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.
        showSkipButton(false);
        setProgressButtonEnabled(false);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);
/*
        mWebTrackerCore = WebTrackerCore.getInstance(getApplicationContext());
        mServiceStatusListener = new ServiceStatusListener() {
            @Override
            public void onServiceConnected() {
                Log.i(TAG, ">>>>>>>> Configuring the service...");
                InitServiceTask task = new InitServiceTask();
                task.execute();
            }

            @Override
            public void onServiceDisconnected(int serviceError_) {
                Log.i(TAG, ">> Service disconected. Code: "+serviceError_);
            }
        };*/
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
           finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        setResult(Activity.RESULT_OK);
        prefs.edit().putBoolean(GlobalSettings.WIZARD_PASSED,true).commit();
        goToCalibration();

    }
    private void goToCalibration() {
        //Intent intent = new Intent(WizardActivity.this, CalibrationActivity.class);
        //this.startActivityForResult(intent, CALIBRATION_ACTIVITY);
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        slidePosition=mPagerAdapter.getFragments().indexOf(newFragment);

        if(slidePosition==SLIDE_VERIFICATION_INDEX){
        }else
        if(slidePosition==SLIDE_LOCATOR_INDEX){
            setSwipeLock(true);

        }else
        if(newFragment instanceof WizardCheckMinimalsFragment){

            if(WizardCheckMinimalsFragment.qualityPassed()){
                setProgressButtonEnabled(true);
                prefs.edit().putBoolean(GlobalSettings.MINIMALS_PASSED,true).commit();

            }else{
                prefs.edit().putBoolean(GlobalSettings.MINIMALS_PASSED,false).commit();
                showSkipButton(true);
            }
        }else
        if(slidePosition==SLIDE_POSITION_INDEX){
            ((WizardCameraLocatorFragment)oldFragment).getLocatorDistance();
        }


    }

    @Override
    public void onNextPressed(){
        setSwipeLock(false);
        if(slidePosition==SLIDE_LOCATOR_INDEX){
            pager.goToNextSlide();
            cameraLocatorFragment.getLocatorDistance();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALIBRATION_ACTIVITY) {
            fullStartEngine(getApplicationContext());
            finish();
        }

    }

    static void fullStartEngine(Context c) {
        AccessibilityServiceModeEngine engine= EngineSelector.getAccessibilityServiceModeEngine();
        if (engine!= null && engine.isReady()) {
            engine.enableAll();
            engine.start();

            // Notify AccessibilityServiceModeEngineImpl the wizard has finished
            Intent intent = new Intent( "wizard-closed-event");
            LocalBroadcastManager.getInstance(c).sendBroadcast(intent);
        }
    }




}