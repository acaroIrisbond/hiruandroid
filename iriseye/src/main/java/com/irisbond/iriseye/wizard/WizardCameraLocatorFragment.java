package com.irisbond.iriseye.wizard;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.irisbond.iriseye.BuildConfig;
import com.irisbond.iriseye.R;
import com.irisbond.iriseye.util.GlobalSettings;


public class WizardCameraLocatorFragment extends Fragment {
    LinearLayout cameraLocator;
    RelativeLayout cameraLocatorParent;
    RelativeLayout canvas;
    SharedPreferences prefs;
    private static final float INCH_TO_DECIMETER_CONVERSION=0.254f;


    public WizardCameraLocatorFragment() {
        // Required empty public constructor
    }

    public static WizardCameraLocatorFragment newInstance() {
        WizardCameraLocatorFragment fragment = new WizardCameraLocatorFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.activity_camera_finder, container, false);
        prefs= PreferenceManager.getDefaultSharedPreferences(getContext());

        cameraLocator= (LinearLayout)v.findViewById(R.id.cameraLocator);
        cameraLocatorParent= (RelativeLayout)v.findViewById(R.id.cameraLocatorParent);
        canvas= (RelativeLayout)v.findViewById(R.id.canvas);

        canvas.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                float touchX = event.getX();

                float touchY = event.getY();

                cameraLocatorParent.invalidate();
                cameraLocatorParent.setTranslationX(touchX);


                return  true;
            }


        });
        Button btnAccept=(Button)v.findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.camera_location_confirm))
                        .setCancelable(true)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getLocatorDistance();
                            }
                        });
                AlertDialog alert = builder.create();

                alert.show();

            }
        });
        return v;
    }


    public void getLocatorDistance() {
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int[] loc = new int[2];
        cameraLocator.getLocationOnScreen(loc);
        int distance = dm.widthPixels - loc[0];

        float decimentersDistance=(distance/dm.xdpi)*INCH_TO_DECIMETER_CONVERSION;
        prefs.edit().putFloat(GlobalSettings.CAMERA_DISTANCE_TO_BORDER,decimentersDistance).commit();

        if (BuildConfig.DEBUG) {
        Toast.makeText(getContext(),"Distance To border "+decimentersDistance, Toast.LENGTH_LONG).show();
        }







    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
