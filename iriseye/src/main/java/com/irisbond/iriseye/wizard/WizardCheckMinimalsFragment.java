package com.irisbond.iriseye.wizard;

import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.irisbond.iriseye.R;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;


public class WizardCheckMinimalsFragment extends Fragment {


    TextView lbQualityCheckText;
    ImageView imgQualityCheck;

    public static final float MINIMAL_CAMERA_RESOLUTION = 2f;
    public static final int MINIMAL_NUMBER_OF_CORES=4;



    public WizardCheckMinimalsFragment() {
        // Required empty public constructor
    }

    public static WizardCheckMinimalsFragment newInstance() {
        WizardCheckMinimalsFragment fragment = new WizardCheckMinimalsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_wizard_check_minimals, container, false);
        lbQualityCheckText=(TextView)v.findViewById(R.id.lbQualityCheckText);
        imgQualityCheck=(ImageView)v.findViewById(R.id.imgQualityCheck);
        if(qualityPassed()){
            imgQualityCheck.setImageResource(R.drawable.ok);
            lbQualityCheckText.setText(getText(R.string.quality_check_passed));
        }else{
            imgQualityCheck.setImageResource(R.drawable.wrong);
            lbQualityCheckText.setText(getText(R.string.quality_check_not_passed));
        }
        return v;
    }

    public static boolean qualityPassed() {
        boolean passed=false;
        if(getBackCameraResolutionInMp()>=MINIMAL_CAMERA_RESOLUTION && getNumberOfCores()>=MINIMAL_NUMBER_OF_CORES){
            passed=true;
        }

        return passed;
    }


    public static int getNumberOfCores(){
        if(Build.VERSION.SDK_INT >= 17) {
            return Runtime.getRuntime().availableProcessors();
        }
        else {
            return getNumCoresOldPhones();
        }
    }

    /**
     * Gets the number of cores available in this device, across all processors.
     * Requires: Ability to peruse the filesystem at "/sys/devices/system/cpu"
     * @return The number of cores, or 1 if failed to get result
     */
    private static int getNumCoresOldPhones() {
        //Private Class to display only CPU devices in the directory listing
        class CpuFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                //Check if filename is "cpu", followed by a single digit number
                if(Pattern.matches("cpu[0-9]+", pathname.getName())) {
                    return true;
                }
                return false;
            }
        }

        try {
            //Get directory containing CPU info
            File dir = new File("/sys/devices/system/cpu/");
            //Filter to only list the devices we care about
            File[] files = dir.listFiles(new CpuFilter());
            //Return the number of cores (virtual CPU devices)
            return files.length;
        } catch(Exception e) {
            //Default to return 1 core
            return 1;
        }
    }

    public static float getBackCameraResolutionInMp()
    {
        int noOfCameras = Camera.getNumberOfCameras();
        float maxResolution = -1;
        long pixelCount = -1;
        for (int i = 0;i < noOfCameras;i++)
        {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
            {
                Camera camera = Camera.open(i);;
                Camera.Parameters cameraParams = camera.getParameters();
                for (int j = 0;j < cameraParams.getSupportedPictureSizes().size();j++)
                {
                    long pixelCountTemp = cameraParams.getSupportedPictureSizes().get(j).width * cameraParams.getSupportedPictureSizes().get(j).height; // Just changed i to j in this loop
                    if (pixelCountTemp > pixelCount)
                    {
                        pixelCount = pixelCountTemp;
                        maxResolution = ((float)pixelCountTemp) / (1024000.0f);
                    }
                }

                camera.release();
            }
        }

        return maxResolution;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
