
IRISEYE eye Mouse allows the user to **control an Android&trade; device by tracking the eye**.

Based on the movement of the face captured through the **front camera**, the app allows the user to control a pointer on the screen (i.e., like a mouse), which provides direct access to most elements of the user interface.



## For whom is IRISEYE?

IRISEYE is intended for those who cannot use a touchscreen. For instance, some people with amputations, cerebral palsy, spinal cord injury, muscular dystrophy, multiple sclerosis, amyotrophic lateral sclerosis (ALS) or other disabilities may benefit from this app.

## Requirements

* Mobile phone or tablet
* Android 4.1 (Jelly Bean) or higher

* Dual-core processor or higher


## Limitations

Due to pre-existing restrictions of the Android platform, there are currently some limitations.


* For Android versions prior to 5.1 most standard keyboards do not work with IRISEYE, so a basic keyboard is provided. Such a keyboard needs to be manually activated after the installation.
* Does not work with most games. 
* Browsers do not handle properly some actions (we recommend using Google Chrome).
* Applications such as Maps, Earth and Gallery work with restrictions.

For obvious reasons, IRISEYE has not been tested with all devices available on the market. If you find any issues with your device, please, let us know.

## FAQ
### Can I use a regular keyboard?
For Android 5.1 and higher it seems that [GBoard](https://play.google.com/store/apps/details?id=com.google.android.inputmethod.latin) works well with IRISEYE.

### I receive the message "Because an app is obscuring a permission request, Settings can't verify your response". What can I do?
This is due to other apps having the "Draw over other apps" permission granted. Try the following:
- In Settings > Apps > Configure apps (Gear Icon) > Draw over other apps
- Disable the permission one by one for all apps other than IRISEYE to find out which one interferes with IRISEYE.

## Thanks



